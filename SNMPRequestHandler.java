package com.dvsoft.nexus.service.systemmanagement.snmp;

import snmp.*;

import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;
import java.text.SimpleDateFormat;

import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPTreeNexus;
import com.dvsoft.nexus.service.SRTEException;
import com.dvsoft.nexus.versiontracker.VersionObserver;
import com.dvsoft.nexus.versiontracker.VersionTracker;
import com.dvsoft.trace.Trace;

/**
 * Abstract class that the SNMPRequests will be deligated to.  Implementations of this interface will register them self
 * with the SNMPMainDeligator indicating what OIDs it can handle. <br/>
 * Implementations exist for:
 * <ol>
 *  <li>handled OID registration (overiding class only need to indicate OIDs or OID ranges):
 *      <ol>
 *          <li><code>getOIDsForThisHandler()</code></li>
 *          <li><code>addOIDRange()</code></li>
 *      </ol>
 *  </li>
 *  <li>Logic for determining the next OID (either from overiding class or SNMPTree.</li>
 *  <li>Version tracking on the SystemProperty table of Nexus.</li>
 *  <li>Utility methods for shared functions possibly needed by overiding classes:
 *      <ol>
 *          <li><code>calculateNextOIDForTable()</code></li>
 *          <li><code>extractColumn()</code></li>
 *          <li><code>extractRow()</code></li>
 *      </ol>
 *  </li>
 * </ol>
 */
public abstract class SNMPRequestHandler implements VersionObserver {
    /** Set to: "yyyy-MM-dd,hh:mm:ss.S"  This seems to be the standard for dates in SNMP (see AdventNet web-page) */
    public static SimpleDateFormat dateTimeFormatter = new SimpleDateFormat ("yyyy-MM-dd,hh:mm:ss.0");
    private static final String SYSTEM_PROPERTY_TABLE_NAME = "SystemProperty";

    private static Trace trace = Trace.get(SNMPRequestHandler.class);

    private ArrayList oidList = new ArrayList();
    protected SNMPInteractionToNexus nexus;
    protected VersionTracker versionTracker;

    /** default constructor */
    public SNMPRequestHandler(SNMPInteractionToNexus nexus) {
        this.nexus = nexus;
    }

    /**
     * All OID's this handler knows how to handle.  The SNMPMainDeligator will request this information when the handler is
     * registered.
     *
     * @return Array of snmp.SNMPObjectIdentifier.  Not guaranteed to be ordered in any way.
     * @see SNMPRequestHandler#addOIDRange
     */
    public Object[] getOIDsForThisHandler() {
        return oidList.toArray();
    }

    /**
     * Implementing classes need (to during creation) add all the OID into the range of OID's.  This method is
     * used to add oid ranges.  By adding the OID here, the delegating class will know to invoke the relevant
     * methods for OID's in the range to this instance.<br/>
     * The range should be in the format x.y.z.* where the wildcard indicates all sub-branches.
     */
    protected void addOIDRange(String oid) {
        oidList.add(oid);
    }

    /**
     * For retrieving the next OID, it is assumed that in tables the given OID of a leave node may contain
     * an additional index for each row.  These will typically be a zero based index (0 for the first row,
     * 1 for the next etc.). <br/><br/>
     * This method will also be invoked by the SNMPMainDeligator when it requires to know the next OID.  First the
     * implementing class will be asked if it knows what the next OID is (via the
     * <code>getNextOIDIfKnown() method</code>), if it returns null, the MIB tree is used (e.g. SNMPTreeNexus) to
     * get the next OID. If it is still not found null is returned (this should only happen if this application
     * does not handle any more OID's).
     *
     * @see #getNextOIDIfKnown
     * @see com.dvsoft.nexus.service.systemmanagement.snmp.SNMPTreeNexus
     */
    public SNMPObjectIdentifier getNextOID(SNMPObjectIdentifier oid) {
        SNMPObjectIdentifier nextOID = getNextOIDIfKnown(oid);

        if (nextOID == null) {
        	nextOID = SNMPTreeNexus.getInstance().getNextOID(oid);
        }
        return nextOID;
    }

    /**
     * Activate is typically used if the handler needs to activate a source of data before becoming active.<br/>
     * Always execute this method. i.e. do a super.activate() when overiding.
     */
    public void activate() {
        try {
            versionTracker = (VersionTracker) nexus.getSrte().getNamedService(VersionTracker.IID_VersionTracker);
        } catch (SRTEException e) {
            trace.severe("Could not retrieve the VersionTracker from the SRTE: ", e);
        }

        versionTracker.registerObserver(this, SYSTEM_PROPERTY_TABLE_NAME);
    }

    /**
     * Stop processing (stop threads).<br/>
     * Always execute this method. i.e. do a super.deactivate() when overiding.
     */
    public void deactivate() {
        versionTracker.unregisterObserver(this);
    }

    /**
     * If the implementing class has a preference to what should be returned as the next OID given a OID, it
     * should be implemented here.  This is typically used in tables where the next OID is only known
     * to the class that knows how many rows there are.
     *
     * @return return the next OID if known, else return null
     */
    protected abstract SNMPObjectIdentifier getNextOIDIfKnown(SNMPObjectIdentifier oid);

    /**
     * Retrieve the value for a given OID.  This can be for either nodes, leave nodes, or leave nodes in tables.
     * @return If the specific requestHandler does not have a value for this OID, return null.
     */
    public abstract SNMPObject getValueForOID(SNMPObjectIdentifier oid) throws SNMPGetException;

    /**
     * Set the value from the PDU.  This can be for either nodes, leave nodes, or leave nodes in tables.
     * Since for a table row all columns will be in one pdu, this method differs from the get mthods that always
     * handle one oid only.<br/>
     *
     * @throws SNMPSetException when the OID is read-only.
     */
    public abstract SNMPVarBindList setValueForOID(SNMPSequence pdu) throws SNMPSetException;

    /**
     * Utility method that will extract the column from the OID.  If the column can not be extracted (i.e. the index
     * is beyond the length of the OID, zero is returned.
     *
     * @param columnIndex Zero based index into the OID.  extractColumn(2, new SNMPObjectIdentifier("x.y.z"))
     * will return y.
     * @param oid
     * @return the integer located at <code>columnIndex</code> into <code>oid</code>
     */
    protected static int extractColumn(int columnIndex, SNMPObjectIdentifier oid) {
        long[] nodes = (long[]) oid.getValue();

        if (nodes.length > columnIndex) {
        	return (int)nodes[columnIndex];
        }
        return 0;
    }

    /**
     * Same logic as extractColumn.
     * @see #extractColumn
     */
    protected static int extractRow(int rowIndex, SNMPObjectIdentifier oid) {
        // These do exactly the same at this stage.
        return extractColumn(rowIndex, oid);
    }

    /**
     * Utility Method to calculate the next OID from an OID within the a table.
     *
     * @param currentOid This OID has to be within the table we are working with, i.e. <code>baseTableOID + ".1.1"</code>
     * for col 1 or <code>baseTableOID + "1.3.2" for col 3 row 2
     * @param baseTableOID The OID for the table that we are working with.  <i>1.3.6.1.4.1.1484.4.3.2.4.3</i> for example.
     * @param nodesInBaseTableOID Added for performance, since in most cases the number of nodes in the baseTableOID is
     * known this is inserted here so as to avoid having to use StringTokenizer.  Will be <i>12</i> for the
     * TableOID <i>1.3.6.1.4.1.1484.4.3.2.4.3</i>.
     * @param columnCount The number of columns in the table.
     * @param rowCount The number of rows that we want to return.
     * @return an OID where the row is one larger than the previous unless the rowCount has been reached.  If the
     * previous row was empty (first getNext request) the row will now be 1 (therefore 1-based).  If the the next
     * OID can not be calculated, null is returned.
     * @throws SNMPBadValueException
     */
    protected SNMPObjectIdentifier calculateNextOIDForTable(SNMPObjectIdentifier currentOid,
                                                  String baseTableOID,
                                                  int nodesInBaseTableOID,
                                                  int columnCount,
                                                  int rowCount)
            throws SNMPBadValueException {
        if (currentOid.toString().startsWith(baseTableOID + ".1")) {  // 13 values
            int row = 0, column;
            //System.out.println("calculateNextOIDForTable");

            if ((column = extractColumn(nodesInBaseTableOID + 1, currentOid)) > 0) {
                row = extractRow(nodesInBaseTableOID + 2, currentOid) + 1;  // zero returned if not existant.
                //System.out.println("The row value is :"+row);

                if (column <= columnCount) {
                    if (row <= rowCount) {  // Do we want them to ask for another row?
                        return new SNMPObjectIdentifier(baseTableOID + ".1." + column + "." + row);
                    }
                }  // if larger than the last column, return null...
            }
        }
       return null;
    }

    /**
     * Callback method for observing the VersionTracker.  This method is invoked when there is a change to the
     * SystemProperty table in nexus. <br/>
     * This method then invokes the <code>refreshSystemProperties()</code> method on the implementing classes.
     *
     * @see #refreshSystemProperties
     */
    public void versionsChanged(VersionTracker tracker, Set items) {
        for (Iterator i = items.iterator(); i.hasNext();) {
            String s = (String) i.next();
            if ((SYSTEM_PROPERTY_TABLE_NAME).equals(s)) {
                this.refreshSystemProperties();
            }
        }
    }

    /**
     * Implementing classes should do all configuration updates from this method.
     */
    protected abstract void refreshSystemProperties();
}
