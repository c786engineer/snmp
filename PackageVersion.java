/*****************************************************************************
 * Copyright   : Copyright (C) 2004, DataVoice PTY. (LTD).                   *
 *               All rights reserved.                                        *
 *                                                                           *
 * Program     : PackageVersion.java                                         *
 *                                                                           *
 *     This file was updated by VersionMaintainer at:                        *
 *     Fri Oct 28 14:59:04 GMT+02:00 2005
 *                                                                           *
 *****************************************************************************/

package com.dvsoft.nexus.service.systemmanagement.snmp;

public class PackageVersion extends com.dvsoft.util.Version {
   public int getMajor() {
      return 1;
   }
   public int getMinor() {
      return 0;
   }
   public int getBugFix() {
      return 0;
   }
   public int getBuild() {
      return 4;
   }
}
