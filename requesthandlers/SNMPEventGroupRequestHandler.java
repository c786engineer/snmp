package com.dvsoft.nexus.service.systemmanagement.snmp.requesthandlers;

import snmp.*;

import com.dvsoft.trace.Trace;
import com.dvsoft.nexus.systemmanagement.AlarmEvent;
import com.dvsoft.nexus.event.AlarmEventAction;
import com.dvsoft.nexus.event.AlarmEventType;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPInteractionToNexus;

import java.util.Vector;
import java.math.BigInteger;

/**
 * Abstract class purely for minimising code duplication.  This class provides all the functionality for presenting
 * a event group filtered view of the alarms.  The overriding classes will only provide instance specific storage and
 * constants.<br/><br/>
 * Constants:
 * <ol>
 *  <li>what is the Base OID</li>
 *  <li>what is the filter to apply</li>
 * </ol>
 * State:
 * <ol>
 *  <li>alarms, a cached Vector of the alarms</li>
 *  <li>max rows, the maximum number of rows to return in the table.</li>
 *  <li>previous time, the time alarms was last refreshed.</li>
 * </ol>
 */
public abstract class SNMPEventGroupRequestHandler extends SNMPRequestHandler {
    protected static final String SNMP_EVENT_GROUP_DATABASE             = "db";
    protected static final String SNMP_EVENT_GROUP_GENERAL_SERVER       = "general";
    protected static final String SNMP_EVENT_GROUP_LICENSING            = "lic";
    protected static final String SNMP_EVENT_GROUP_MEDIA                = "media";
    protected static final String SNMP_EVENT_GROUP_RECORDER_IP          = "recIP";
    protected static final String SNMP_EVENT_GROUP_RECORDER_ORION       = "recOrion";
    protected static final String SNMP_EVENT_GROUP_RECORDING_CONTROLLER = "rc";
    protected static final String SNMP_EVENT_GROUP_STORAGE              = "storage";

    private static final int OID_COLUMN_INDEX = 14;
    private static final int OID_ROW_INDEX = 15;
    private int cacheLifeSec = 10;

    // state or constants specific to event group being implemented.
    protected static Trace trace = Trace.get(SNMPEventGroupRequestHandler.class);

    public SNMPEventGroupRequestHandler(SNMPInteractionToNexus nexus) {
        super(nexus);
        super.addOIDRange(getBaseOID() + ".*");
    }

    public void activate() {
        super.activate();
        refreshAlarms();
    }

    /**
     * @return Since we are working with a table here, we need to figure out what cell we we're in and
     * return the next cell if available.  If the end has been reached, null is returned.
     *
     * @see SNMPRequestHandler#getNextOID  for more on how the null is handled by the super.
     */
    protected SNMPObjectIdentifier getNextOIDIfKnown(SNMPObjectIdentifier oid) {
        try {
            refreshAlarms();
            return calculateNextOIDForTable(oid, getBaseOID() + ".3", 13, 16, getMaxRows() < getAlarms().size() ? getMaxRows() : getAlarms().size());
        } catch (SNMPBadValueException e) {
            trace.severe("Error converting a String to a OID: ", e);
        }
        return null;
    }

    public SNMPObject getValueForOID(SNMPObjectIdentifier oid) throws SNMPGetException {
        if (oid.toString().startsWith(getBaseOID() + ".1.0")) {
            return new SNMPInteger(getMaxRows());
        } else if (oid.toString().startsWith(getBaseOID() + ".2.0")) {
            return new SNMPInteger(getAlarms().size());
        } else if (oid.toString().startsWith(getBaseOID() + ".3.1")) {  // 13 values
            int row = 0, column;

            if ((column = extractColumn(OID_COLUMN_INDEX, oid)) > 0) {
                row = extractRow(OID_ROW_INDEX, oid) - 1;  // zero returned if not existant.

                if (row >= 0) {  // row of -1 = reached the end...
                    refreshAlarms();
                    AlarmEvent alarm = (AlarmEvent) getAlarms().get(row);

                    switch (column) {
                        case 1: // alleEventIndex (typically not requested)
                            return new SNMPInteger(row);
                        case 2: // alleEventIndex (typically not requested)
                            return new SNMPInteger(alarm.getID());
                        case 3: // alleEventUrl
                            return new SNMPOctetString(alarm.getEventType().getURI());
                        case 4: // alleLastChangeDateTime
                        	System.out.println(SNMPRequestHandler.dateTimeFormatter.format(alarm.getDateTime()));
                            return new SNMPOctetString(SNMPRequestHandler.dateTimeFormatter.format(alarm.getDateTime()));
                        case 5: // alleEventGroup
                          String group = nexus.getEventGroup(alarm.getEventType().getURI());
                          return new SNMPOctetString(group != null ? group : "unknown URI");
                        case 6: // alleName
                            String name = nexus.getName(alarm.getEventType().getURI(), alarm.getParameters());
                            return new SNMPOctetString(name != null ? name : "unknown URI");
                        case 7: // alleDescription
                            String message = nexus.getMessage(alarm.getEventType().getURI(), alarm.getParameters());
                            return new SNMPOctetString(message != null ? message : "unknown URI");
                        case 8: // alleStatus
                        	return new SNMPOctetString(alarm.getAction().toString());
//                            if (alarm.getAction().equals(AlarmEventAction.FIRED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                                //return new SNMPCounter32(1);
//                            } else if (alarm.getAction().equals(AlarmEventAction.UPDATED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPCounter32(2);
//                            } else if (alarm.getAction().equals(AlarmEventAction.CLEARED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                                //return new SNMPCounter32(3);
//                            } else if (alarm.getAction().equals(AlarmEventAction.FIRED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                                //return new SNMPCounter32(4);
//                            } else {
//                            	return new SNMPOctetString("unknown");
//                                //return new SNMPCounter32(5);  // unknown
//                            }
                        case 9: // alleContinues
                            if (alarm.getEventType() instanceof AlarmEventType) {
                                if (((AlarmEventType) alarm.getEventType()).isContinuous()) {
                                    return new SNMPInteger(1);
                                } else {
                                    return new SNMPInteger(2);
                                }
                            }
                        case 10: // alleSourceHost
                            return new SNMPOctetString(alarm.getSourceLocation().getHostName());
                        case 11: // alleSourceHostType
                            return new SNMPOctetString(alarm.getSourceLocation().getHostType());
                        case 12: // alleSourceProcess
                            return new SNMPOctetString(alarm.getSourceLocation().getProcessName());
                        case 13: // alleSourceProcessType
                            return new SNMPOctetString(alarm.getSourceLocation().getProcessType());
                        case 14: // alleSourceService
                            return new SNMPOctetString(alarm.getSourceLocation().getServiceName());
                        case 15: // alleSourceServiceType
                            return new SNMPOctetString(alarm.getSourceLocation().getServiceType());
                        case 16: // alleRowStatus
                            return new SNMPInteger(0);
                        default:
                            return new SNMPOctetString("unknown column...");
                    }
                }
            }
        }
        return null;
    }

    private void refreshAlarms() {
        long time = System.currentTimeMillis();

        // since these filtered models are relatively expensive to build, only get if it's older than 10sec
        if (time > getPreviousTime() + (cacheLifeSec*1000)) {
            setAlarms(nexus.getAlarmEvents(getEventGroupImplemented()));
            setPreviousTime(time);
        }
    }

    /**
     * Set the maxRows.
     */
    public SNMPVarBindList setValueForOID(SNMPSequence pdu) throws SNMPSetException {
        SNMPSequence oidPair = (SNMPSequence) pdu.getSNMPObjectAt(0);
        SNMPObjectIdentifier oid = (SNMPObjectIdentifier) oidPair.getSNMPObjectAt(0);
        SNMPObject value = oidPair.getSNMPObjectAt(1);

        if (oid.toString().startsWith(getBaseOID() + ".1.0")) {
            setMaxRows(((BigInteger) value.getValue()).intValue());
        } else {
            throw new SNMPSetException("Setting is not supported for this OID", 0, SNMPRequestException.VALUE_READ_ONLY);
        }

        SNMPVarBindList ret = new SNMPVarBindList();
        try {
            ret.addSNMPObject(new SNMPVariablePair(oid, value));
        } catch (SNMPBadValueException e) {
            trace.severe("Unexpected value returned from setting value: [" + value + "]", e);
        }
        return ret;
    }

    protected void refreshSystemProperties() {
        setMaxRows(Integer.parseInt(nexus.getNexusConfigurationProperty("SNMPMaxRowCount")));
        cacheLifeSec = Integer.parseInt(nexus.getNexusConfigurationProperty("SNMPTrapPollInterval"));
    }

    protected abstract String getBaseOID();
    protected abstract String getEventGroupImplemented();
    protected abstract Vector getAlarms();
    protected abstract void setAlarms(Vector alarms);
    protected abstract int getMaxRows();
    protected abstract void setMaxRows(int maxRows);
    protected abstract long getPreviousTime();
    protected abstract void setPreviousTime(long previousTime);
}
