package com.dvsoft.nexus.service.systemmanagement.snmp;

import com.dvsoft.nexus.systemmanagement.server.ServerCore;
import com.dvsoft.net.http.client.ConnectionServiceFactory;
import com.dvsoft.net.http.client.ConnectionService;
import com.dvsoft.nexus.systemmanagement.server.Infrastructure;
import com.dvsoft.nexus.systemmanagement.SystemManagementException;
import com.dvsoft.nexus.systemmanagement.AlarmEvent;
import com.dvsoft.nexus.service.SRTE;
import com.dvsoft.nexus.service.srte.ConfigUtils;
import com.dvsoft.util.db.DBClient;
import com.dvsoft.util.db.DBClientFactory;
import com.dvsoft.util.db.DBException;
import com.dvsoft.util.db.SimpleResultSet;
import com.dvsoft.config.ConfigNode;
import com.dvsoft.trace.Trace;


import java.util.*;

import snmp.SNMPTimeTicks;

/**
 * All interactions the SNMP classes has to Nexus classes and in most cases data happen via this
 * implementation.  This is not an SRTE service, instead is implements Infrastructure in order to
 * use the ServerCore (source of alarms and events).
 */
public class SNMPInteractionToNexusImpl implements SNMPInteractionToNexus, Infrastructure {
    private ServerCore serverCore;

    // For infrastructure interface impl.
    private ConfigNode configNode;
    private ConnectionService connectionService;
    private DBClient dbClient;
    private SRTE srte;      // Need to store it purely for the getWorkingDir Method.

	
    private static SNMPInteractionToNexusImpl instance; // one way to do a singleton.

    private static final String CONFIG_NODE_DB_CLIENT_FACTORY = "DBClientFactory";
    private static Trace trace = Trace.get(SNMPInteractionToNexusImpl.class);

    /**
     * No public default constructor, can not function without SRTE and props...
     */
    private SNMPInteractionToNexusImpl() {
    }

    /**
     * This constructor takes all that is need to function with the ServerCore.
     */
    private SNMPInteractionToNexusImpl(SRTE srte, Properties properties) {
        try {
            this.srte = srte;

            configNode = ConfigUtils.getMyConfigNode(srte, properties);


            String dbClientFactoryInstanceName = configNode.getNodeStringValue(CONFIG_NODE_DB_CLIENT_FACTORY, null);

            DBClientFactory dbClientFactory = (DBClientFactory) srte.getNamedService(
                    DBClientFactory.IID_DBClientFactory, dbClientFactoryInstanceName);

            dbClient = dbClientFactory.createDBClient(false);

            connectionService = ConnectionServiceFactory.getInstance().getConnectionService(null, false);

            serverCore = ServerCore.getInstance();

            serverCore.initialise(this);
           
        } catch (Exception e) {

        }
    }

    /**
     * The SRTE makes part of the core system... (used to get the VersionTracker at this stage)
     */
    public SRTE getSrte() {
        return srte;
    }

    /**
     * Singleton instance.
     */
    public static SNMPInteractionToNexus getInstance(SRTE srte, Properties properties) {
        if (instance == null) {
            instance = new SNMPInteractionToNexusImpl(srte, properties);
        }

        return instance;
    }

    public void activate() {
        serverCore.activate();
    }

    public void deactivate() {
        serverCore.deactivate();
    }

    public void destroy() {
        serverCore.destroy();
    }

    /**
     * Gets the Cont Alarms from the HealthModel using the ServerCore.
     * @see com.dvsoft.nexus.systemmanagement.HealthModel#getContinuousAlarms
     */
    public Vector getContinuousAlarms() {
        return serverCore.getCurrentHealthModel().getContinuousAlarms();
    }

    /**
     * Gets all the alarms events from the HealthModel using the ServerCore.
     * @see com.dvsoft.nexus.systemmanagement.HealthModel#getAlarmEvents
     */
    public Vector getAlarmEvents() {
        return serverCore.getCurrentHealthModel().getAlarmEvents();
    }

    /**
     * Given a event uri, find the SNMP event group this event belongs to.  Use the event dictionary to resolve this.
     * @see com.dvsoft.nexus.systemmanagement.util.BasicDictionary#getSNMPGroup
     */
    public String getSnmpEventGroup(String eventURI) {
        try {
            return serverCore.retrieveStaticData().getEventDictionary().getSNMPGroup(eventURI);
        } catch (SystemManagementException e) {
            trace.severe("Problem retrieving group data from dictionary: ", e);
            return null;
        }
    }

  /**
   * Given a event URI, find the standard event group this event belongs to.
   * Exceptions trapped here.
   */
  public String getEventGroup(String eventURI) {
    try {
        return serverCore.retrieveStaticData().getEventDictionary().getGroup(eventURI);
    } catch (SystemManagementException e) {
        trace.severe("Problem retrieving group data from dictionary: ", e);
        return null;
    }
  }

  /**
     * Retrieve all the alarms events for the specified SNMP event group.  This method is implemented by retrieving
     * all events, then events from the specified group is copied into a new Vector.   This method is rather
     * expensive (it's recommended to cache the Vector for 10seconds or so).
     *
     * @see SNMPInteractionToNexusImpl#getAlarmEvents
     */
    public Vector getAlarmEvents(String snmpEventGroup) {
        Vector allAlarms = getAlarmEvents();
        Vector filteredAlarms = new Vector();

        for (int i = 0; i < allAlarms.size(); i++) {
            AlarmEvent event = (AlarmEvent) allAlarms.elementAt(i);

            String group = getSnmpEventGroup(event.getEventType().getURI());

            if (snmpEventGroup.equalsIgnoreCase(group)) {
                filteredAlarms.add(event);
            }
        }

        return filteredAlarms;
    }

    /**
     * Retrieve the lexographical name for the event given the event URI.  Implementation retrieves it from
     * the Dictionary.
     *
     * @see com.dvsoft.nexus.systemmanagement.util.BasicDictionary#getName with language set to <code>en<code/>
     */
    public String getName(String eventURI, String[] param) {
        try {
            return serverCore.retrieveStaticData().getEventDictionary().getName(eventURI, param, "en");
        } catch (SystemManagementException e) {
            trace.severe("Problem retrieving name data from dictionary: ", e);
            return null;
        }
    }

    /**
     * Retrieve the lexographical message for the event given the event URI with the parameters substituted.
     * Implementation retrieves it from the Dictionary.
     *
     * @see com.dvsoft.nexus.systemmanagement.util.BasicDictionary#getMessage with language set to <code>en<code/>
     */
    public String getMessage(String eventURI, String[] param) {
        try {
        	return serverCore.retrieveStaticData().getEventDictionary().getMessage(eventURI, param, Locale.getDefault().getLanguage());
        } catch (SystemManagementException e) {
            trace.severe("Problem retrieving message data from dictionary: ", e);
            return null;
        }
    }

    /**
     * retrieve configuration items from nexus.  This implementation does a select on the SystemPropery table.
     * @param propertyName name column
     * @return value column if found, null if it was not found.
     */
    public String getNexusConfigurationProperty(String propertyName) {
        try {
            SimpleResultSet res = dbClient.executeQuery("EXEC [dvsp_GetSystemProperty] @PropertyName = '" + propertyName + "'");

            while (res.next()) {
                return res.getString("Value");
            }
        } catch (DBException e) {
            trace.severe("Error retrieving configuration data from DB: ", e);
        }

        return null;
    }

    /**
     * Retrieve the URI's of all the events that SNMP should monitor (send traps on new occurances).  Implemented
     * as a select query on the SNMPTrapView View.
     *
     * @return list of URIs (String), null if not available.
     */
    public List retrieveSnmpTrapUriS() {
        try {
            List ret = new ArrayList();

            SimpleResultSet res = dbClient.executeQuery("SELECT * FROM SNMPTrapView");

            while (res.next()) {
                ret.add(res.getString("EventTypeURI"));
            }

            return ret;
        } catch (DBException e) {
            trace.severe("Error retrieving configuration data from DB: ", e);
        }

        return null;
    }

    /**
     * The index for the last event in the model at the time of invoking this method.  Changes in the index indicates
     * new events. Gets the HealthModel using the ServerCore.
     * @see com.dvsoft.nexus.systemmanagement.HealthModel#getMaxAlarmEventID
     */
    public long getMaxAlarmEventID() {
        return serverCore.getCurrentHealthModel().getMaxAlarmEventID();
    }

    /**
     * Retrieve all events that occurred from an index. Gets the HealthModel using the ServerCore.
     * @see com.dvsoft.nexus.systemmanagement.HealthModel#getAlarmEventsSince
     */
    public Vector getAlarmEventsSince(long previousMaxAlarmEventID) {
        return serverCore.getCurrentHealthModel().getAlarmEventsSince(previousMaxAlarmEventID);
    }

    /** Infrastructure methods implemented. */
    public ConnectionService getConnectionService() {
        return connectionService;
    }

    /** Infrastructure methods implemented. */
    public DBClient getDBClient() {
        return dbClient;
    }

    /** Infrastructure methods implemented. */
    public ConfigNode getConfigNode() {
        return configNode;
    }

    /** Infrastructure methods implemented. */
    public String getWorkingDir() {
        return srte.getWorkingDir();
    }
    
    
    
   
}
