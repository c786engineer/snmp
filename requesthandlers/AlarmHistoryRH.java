package com.dvsoft.nexus.service.systemmanagement.snmp.requesthandlers;

import snmp.*;
import com.dvsoft.trace.Trace;
import com.dvsoft.nexus.systemmanagement.AlarmEvent;
import com.dvsoft.nexus.event.AlarmEventAction;
import com.dvsoft.nexus.event.AlarmEventType;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPInteractionToNexus;

import java.util.Vector;
import java.math.BigInteger;

/**
 */
public class AlarmHistoryRH extends SNMPRequestHandler {
    private static Trace trace = Trace.get(AlarmHistoryRH.class);
    private Vector alarms;

    private int maxRows = 100;

    private static String baseOID = "1.3.6.1.4.1.1484.1.1.2.2"; // iso.org.dod.internet.private.enterprises.datavoice.libraNexus.alarms.alarmHistory
    private static final int OID_COLUMN_INDEX = 13;
    private static final int OID_ROW_INDEX = 14;

    public AlarmHistoryRH(SNMPInteractionToNexus nexus) {
        super(nexus);
        super.addOIDRange(baseOID + ".*");
    }

    public void activate() {
        super.activate();
        alarms = nexus.getAlarmEvents();
    }

    /**
     * @return Since we are working with a table here, we need to figure out what cell we were in and
     * return the next cell if available.  If the end has been reached, null is returned.
     *
     * @see com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler#getNextOID  for more on how the null is handled by the super.
     */
    protected SNMPObjectIdentifier getNextOIDIfKnown(SNMPObjectIdentifier oid) {
        try {
            alarms = nexus.getAlarmEvents();
            return calculateNextOIDForTable(oid, baseOID + ".2", 12, 16, maxRows < alarms.size() ? maxRows : alarms.size());
        } catch (SNMPBadValueException e) {
            trace.severe("Error converting a String to a OID: ", e);
        }
        return null;
    }

    public SNMPObject getValueForOID(SNMPObjectIdentifier oid) throws SNMPGetException {
        if (oid.toString().startsWith(baseOID + ".1.0")) {
            return new SNMPInteger(maxRows);
        } else if (oid.toString().startsWith(baseOID + ".2.1")) {  // 13 values
            int row = 0, column;

            if ((column = extractColumn(OID_COLUMN_INDEX, oid)) > 0) {
                row = extractRow(OID_ROW_INDEX, oid) - 1;  // zero returned if not existant.

                if (row >= 0) {  // row of -1 = reached the end...
                    alarms = nexus.getAlarmEvents();
                    AlarmEvent alarm = (AlarmEvent) alarms.get(row);

                    switch (column) {
                        case 1: // alleEventIndex (typically not requested)
                            return new SNMPInteger(row);
                        case 2: // alleEventIndex (typically not requested)
                            return new SNMPInteger(alarm.getID());
                        case 3: // alleEventUrl
                            return new SNMPOctetString(alarm.getEventType().getURI());
                        case 4: // alleLastChangeDateTime
                            return new SNMPOctetString(SNMPRequestHandler.dateTimeFormatter.format(alarm.getDateTime()));
                        case 5: // alleEventGroup
                          String group = nexus.getEventGroup(alarm.getEventType().getURI());
                          return new SNMPOctetString(group != null ? group : "unknown URI");
                        case 6: // alleName
                            String name = nexus.getName(alarm.getEventType().getURI(), alarm.getParameters());
                            return new SNMPOctetString(name != null ? name : "unknown URI");
                        case 7: // alleDescription
                            String message = nexus.getMessage(alarm.getEventType().getURI(), alarm.getParameters());
                            return new SNMPOctetString(message != null ? message : "unknown URI");
                        case 8: // alleStatus
                        	return new SNMPOctetString(alarm.getAction().toString());
//                            if (alarm.getAction().equals(AlarmEventAction.FIRED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                                //return new SNMPCounter32(1);
//                            } else if (alarm.getAction().equals(AlarmEventAction.UPDATED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPCounter32(2);
//                            } else if (alarm.getAction().equals(AlarmEventAction.CLEARED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPCounter32(3);
//                            } else if (alarm.getAction().equals(AlarmEventAction.FIRED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPCounter32(4);
//                            } else {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPOctetString("unknown");
//                                //return new SNMPCounter32(5);  // unknown
//                            }
                        case 9: // alleContinues
                            if (alarm.getEventType() instanceof AlarmEventType) {
                                if (((AlarmEventType) alarm.getEventType()).isContinuous()) {
                                    return new SNMPInteger(1);
                                } else {
                                    return new SNMPInteger(2);
                                }
                            }
                        case 10: // alleSourceHost
                            return new SNMPOctetString(alarm.getSourceLocation().getHostName());
                        case 11: // alleSourceHostType
                            return new SNMPOctetString(alarm.getSourceLocation().getHostType());
                        case 12: // alleSourceProcess
                            return new SNMPOctetString(alarm.getSourceLocation().getProcessName());
                        case 13: // alleSourceProcessType
                            return new SNMPOctetString(alarm.getSourceLocation().getProcessType());
                        case 14: // alleSourceService
                            return new SNMPOctetString(alarm.getSourceLocation().getServiceName());
                        case 15: // alleSourceServiceType
                            return new SNMPOctetString(alarm.getSourceLocation().getServiceType());
                        case 16: // alleRowStatus
                            return new SNMPInteger(0);
                        default:
                            return new SNMPOctetString("unknown column...");
                    }
                }
            }
        }
        return null;
    }

    public SNMPVarBindList setValueForOID(SNMPSequence pdu) throws SNMPSetException {
        SNMPSequence oidPair = (SNMPSequence) pdu.getSNMPObjectAt(0);
        SNMPObjectIdentifier oid = (SNMPObjectIdentifier) oidPair.getSNMPObjectAt(0);
        SNMPObject value = oidPair.getSNMPObjectAt(1);

        if (oid.toString().startsWith(baseOID + ".1.0")) {
            maxRows = ((BigInteger) value.getValue()).intValue();
        } else {
            throw new SNMPSetException("Setting is not supported for this OID", 0, SNMPRequestException.VALUE_READ_ONLY);
        }

        SNMPVarBindList ret = new SNMPVarBindList();
        try {
            ret.addSNMPObject(new SNMPVariablePair(oid, value));
        } catch (SNMPBadValueException e) {
            trace.severe("Unexpected value returned from seting value: [" + value + "]", e);
        }
        return ret;
    }

    protected void refreshSystemProperties() {
        maxRows = Integer.parseInt(nexus.getNexusConfigurationProperty("SNMPMaxRowCount"));
    }
}
