package com.dvsoft.nexus.service.systemmanagement.snmp.requesthandlers;

import snmp.*;
import com.dvsoft.trace.Trace;
import com.dvsoft.nexus.systemmanagement.AlarmEvent;
import com.dvsoft.nexus.event.AlarmEventAction;
import com.dvsoft.nexus.event.AlarmEventType;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPInteractionToNexus;

import java.util.Vector;
import java.math.BigInteger;

/**
 */
public class GroupDB_RH extends SNMPEventGroupRequestHandler {
    // state or constants specific to event group beeing implemented.
    protected static Trace trace = Trace.get(GroupDB_RH.class);
    private static String baseOID = "1.3.6.1.4.1.1484.1.1.2.3.1";

    private Vector alarms;
    private int maxRows = 100;
    private long previousTime = 0;

    public GroupDB_RH(SNMPInteractionToNexus nexus) {
        super(nexus);
    }

    protected String getBaseOID() {
        return baseOID;
    }

    protected String getEventGroupImplemented() {
        return SNMP_EVENT_GROUP_DATABASE;
    }


    protected Vector getAlarms() {
        return alarms;
    }

    protected void setAlarms(Vector alarms) {
        this.alarms = alarms;
    }

    protected int getMaxRows() {
        return maxRows;
    }

    protected void setMaxRows(int maxRows) {
        this.maxRows = maxRows;
    }

    protected long getPreviousTime() {
        return previousTime;
    }

    protected void setPreviousTime(long previousTime) {
        this.previousTime = previousTime;
    }

}
