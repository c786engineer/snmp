package com.dvsoft.nexus.service.systemmanagement.snmp;

import snmp.*;

import java.util.*;

import com.dvsoft.trace.Trace;

/**
 * All SNMP request come into this class.  This class then deligates them to the appropriate SNMPRequestHandler.
 * The appropriate SNMPRequestHandler is found by looking it up from the OID-to-Handler map in this class.<br/>
 * All methods expect request for only one OID (as the SNMPMonitor iterates through the PDU).<br/>
 * <br/>
 * This class is not thread-safe, this is OK with the SNMPJava package used as this has only one thread
 * polling for requests.
 *
 * @see com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler
 * @see com.dvsoft.nexus.service.systemmanagement.snmp.SNMPMonitor
 */
public class SNMPMainDeligator {
    private static Trace trace = Trace.get(SNMPMainDeligator.class);

    /** Mapping of OID groups or OIDs to RequestHandler.  An OID can only be mapped to one handler. */
    private Map oidToHandler = new TreeMap();

    /**
     * Activates all the SNMPRequestHandlers that has been registered.
     */
    public void activate() {
        Set handlers = new HashSet(oidToHandler.values());  // Use a set to get only unique handlers

        for (Iterator i = handlers.iterator(); i.hasNext();) {
            SNMPRequestHandler rh = (SNMPRequestHandler) i.next();
            rh.activate();
        }
    }

    /**
     * Deactivates all the SNMPRequestHandlers thats been registered.
     */
    public void deactivate() {
        Set handlers = new HashSet(oidToHandler.values());  // Use a set to get only unique handlers

        for (Iterator i = handlers.iterator(); i.hasNext();) {
            SNMPRequestHandler rh = (SNMPRequestHandler) i.next();
            rh.deactivate();
        }
    }

    /**
     * Only adding supported at this stage (KISS).
     */
    public void addRequestHandler(SNMPRequestHandler handler) {
        Object[] oids = handler.getOIDsForThisHandler();

        for (int i = 0; i < oids.length; i++) {
            Object oid = oids[i];
            oidToHandler.put(oid, handler);
        }
    }

    /**
     * Utility method that looks up the SNMPRequestHandler registered for a OID.  Only one RequestHandler at this stage.
     *
     * @return as above, if none is found returns null
     */
    private SNMPRequestHandler getHandlerForOID(SNMPObjectIdentifier oid) {
        Object handler = oidToHandler.get(oid.toString());

        if (handler != null) {
            return (SNMPRequestHandler) handler;  // use the equals as defined in SNMPObjectIdentifier class.
        }

        // if not found, continue looking at the wildcard. 'oid.*' for ranges
        Set keys = oidToHandler.keySet();
        for (Iterator i = keys.iterator(); i.hasNext();) {
            Object oKey = i.next();
            String key = oKey.toString();
            int index = key.indexOf(".*");
          
            if (index > -1) {
            	//System.out.println("oid: " +oid+ " the handler "+key.substring(0, index));
                if (oid.toString().startsWith(key.substring(0, index))) {
                    return (SNMPRequestHandler) oidToHandler.get(oKey);
                }
            }
        }

        return null;
    }

    /**
     * Simply deligates this request to the registered SNMPRequestHandler.
     *
     * @return value for the OID.  null if this OID is not supported by any deligate.
     * @see SNMPRequestHandler#getValueForOID
     */
    public SNMPObject getRequest(SNMPObjectIdentifier oid) throws SNMPGetException {
        SNMPRequestHandler handler = getHandlerForOID(oid);
        if (handler == null) {
            return null;
        }

        return handler.getValueForOID(oid);
    }

    /**
     * A variable pair, containing the next OID and it's value. <br/>
     * <il>
     * <li>If next OID is found but value for the NextOID can not be found (handler for nextOID can not be
     * found, or handler returns null), the OID and SNMPNull is returned in the pair.
     * <li>If the next OID can not be found (no handler, or OID is out of range), null is returned.
     * </il>
     *
     * @return a oid-value pair for the OID.
     */
    public SNMPVariablePair getNextRequest(SNMPObjectIdentifier oid) throws SNMPGetException {
        // First get the OID
        SNMPRequestHandler handler = getHandlerForOID(oid);
        
        
        try {
        	SNMPObjectIdentifier nextOID = null;
        	SNMPObjectIdentifier currentOID = null;
        	SNMPObject nextValue = null;
        	        	
        	currentOID=oid;
        	nextOID =oid;        	
        	//If the next value is null we must look at the value of the next oid 
        	while((nextValue==null)&&(!currentOID.toString().equalsIgnoreCase(SNMPTreeNexus.finalOID))){
        		handler = getHandlerForOID(currentOID);
        		//If the handler is null we don't have anything to return; but we may mot be at the end of the MIB 
            	if(handler==null){
            		return new SNMPVariablePair(currentOID, new SNMPNull());
            	}
            	nextOID = handler.getNextOID(currentOID);
            	if(nextOID==null){
            		//We are at the end of the MIB
            		return new SNMPVariablePair(currentOID, new SNMPNull());
            	}
            	nextValue = handler.getValueForOID(nextOID);
            	currentOID = nextOID;
        	}
        	if(nextValue==null){
        		return new SNMPVariablePair(nextOID,  new SNMPNull());
        	}else{
        		return new SNMPVariablePair(nextOID, nextValue);
        	}
        } catch (SNMPBadValueException e) {
        	 trace.severe("failed to set the value into the Variable pair: ", e);
        }
        
        
        
        
        
        /**if (handler == null) {
        	//We do not have a handler, but it may not be the end of the MIB. We may still be able to walk it.
        	nextOID = SNMPTreeNexus.getInstance().getNextOID(oid);
        }
        else
        {
        	// get the next OID
        	nextOID = handler.getNextOID(oid);
        	
        	
        }
        
       

        try {
        	 if (nextOID == null) {
        	 	//return new SNMPVariablePair(oid, new SNMPNull());
                return null;
            }
        	 
            // Now return the result from the next OID (if it's valid...)
            handler = getHandlerForOID(nextOID);
            if (handler == null) {
            	return new SNMPVariablePair(nextOID, new SNMPNull());
            }

            SNMPObject nextValue = handler.getValueForOID(nextOID);
            if (nextValue == null) {  // Even though the OID could be in this MIB, the node does not have to return a scalar value.
            	return new SNMPVariablePair(nextOID, new SNMPNull());
            } else {
                return new SNMPVariablePair(nextOID, nextValue);
            }
        } catch (SNMPBadValueException e) {
            trace.severe("failed to set the value into the Variable pair: ", e);
        }*/
        return null;
    }

    /**
     * Takes the first OID from the pdu to find the appropriate handler, then invoke the set on this handler.<br/>
     * This method is different to the get methods in that the PUD (containing a number of set's) is
     * passed in as a parameter, this is because when setting a row in a table, the complete row has to be
     * handled in one go.
     * @throws snmp.SNMPSetException thrown if setting is not supported for the OID (handler
     * not found or OID not a scalar).
     */
    public SNMPVarBindList setRequest(SNMPSequence pdu) throws SNMPSetException {
        // Find the first OID
        SNMPSequence oidPair = (SNMPSequence) pdu.getSNMPObjectAt(0);
        SNMPObjectIdentifier oid = (SNMPObjectIdentifier) oidPair.getSNMPObjectAt(0);

        SNMPRequestHandler handler = getHandlerForOID(oid);
        if (handler == null) {
            throw new SNMPSetException("Setting is not supported for this OID", 0, SNMPRequestException.VALUE_READ_ONLY);
        }

        return handler.setValueForOID(pdu);
    }
}
