/*
 * Copyright   : Copyright (C) 2001, DataVoice PTY. (LTD).
 *               All rights reserved.
 *
 * Program     : SNMPMonitor.java
 * Program By  : Gean Boegman
 * Created     : Fri 06/2004
 *
 * Description : See below in JavaDoc.
 *
 * Revisions   :
 * 2003-05-06 GB. Taken a copy of the HealthMonitor.java to create the SNMPMonitor.java
 */


package com.dvsoft.nexus.service.systemmanagement.snmp;

import java.util.*;
import java.util.logging.Level;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.io.IOException;

import com.dvsoft.nexus.service.SRTE;
import com.dvsoft.nexus.service.SRTEException;
import com.dvsoft.nexus.service.systemmanagement.snmp.requesthandlers.*;
import com.dvsoft.nexus.service.srte.ManagedServiceImpl;
import com.dvsoft.nexus.versiontracker.VersionObserver;
import com.dvsoft.nexus.versiontracker.VersionTracker;
import com.dvsoft.trace.Trace;

// JavaSNMP
import snmp.*;

/**
 * SNMPMonitor listens for SNMP requests and then service them by delegating to the appropriate class.<br/>
 * SNMPMonitor also acts as the central point for dispatching SNMP Traps.<br/>
 * This class is also a tracker for the SystemProperty table, the table where all configurable items are read from.<br/>
 * <br/><code>ManagedServiceImpl</code> gives us a basis for being a DV service (for the SRTE).<br/>
 * <a href=http://edge.mcs.drexel.edu/GICL/people/sevy/snmp/snmp_package.html>See JavaSNMP for more on the JavaSNMP API</a>
 */
public class SNMPMonitor extends ManagedServiceImpl implements SNMPRequestListener, VersionObserver {
    static Trace trace = Trace.get(SNMPMonitor.class);

    private static final String SYSTEM_PROPERTY_TABLE_NAME = "SystemProperty";

    private SNMPInteractionToNexus nexus;
    private SNMPv1AgentInterface snmpAgent;
    private SNMPMainDeligator snmpDeligator;
    private VersionTracker versionTracker;
   
    private Long startupTime =null;
    
    
    //The total amount of snmp packets delivered to this entity by the transport service
    private int snmpInPktsCount = 0;
    //The total number of community-based SNMP messages (for 
    //example,  SNMPv1) delivered to the SNMP entity which 
    //used an SNMP community name not known to said entity.
    private int snmpInBadCommunityNamesCount=0;
    //The total number of ASN.1 or BER errors encountered by 
    //the SNMP entity when decoding received SNMP messages
    private int snmpInASNParseErrsCount=0;
    //The total number of SNMP messages which were delivered 
    //to the SNMP entity and were for an unsupported SNMP 
    //version.
    private int snmpInBadVersionsCount=0;
    //Indicates whether the SNMP entity is permitted to 
    //generate authenticationFailure traps
    //1=enable 2=disable according to RFC
    private int snmpEnableAuthenTrapsValue = 1; //enabled by default 
    public static int SNMPv1 = 0;
    public static int SNMPv2 = 1;
    private int snmpVersion=SNMPv1; //0= snmpv1; 1=snmpv2
    
    // configured items (From SystemProperty table)
    private int SNMP_CLIENT_PORT = 161;
    private String SNMP_COMMUNITY = "public";
    private Set agentsForTrap = new HashSet();
    private String authenticationFailureOID="1.3.6.1.6.3.1.1.5.5";
   

    // ============== Methods from the SNMPRequestListener
    public SNMPSequence processRequest(SNMPPDU snmppdu, int version,String communityName) throws SNMPGetException, SNMPSetException {
    	snmpInPktsCount++;
        if ((SNMP_COMMUNITY.equalsIgnoreCase(communityName))&&(snmpVersion==version)) {
            return muxReqGetSetGetnext(snmppdu);
        }else{
        	if(snmpVersion!=version){
        		snmpInBadVersionsCount++;
        	}
        	if (!SNMP_COMMUNITY.equalsIgnoreCase(communityName)){
        		snmpInBadCommunityNamesCount++;
        		
        		if(snmpEnableAuthenTrapsValue==1){
        			try {
                  		sendSNMPTrap(new SNMPv2TrapPDU(new SNMPObjectIdentifier(authenticationFailureOID),getSysUptime()));
                  		throw new SNMPGetException(0,SNMPRequestException.FAILED);
                  	} catch (SNMPBadValueException e) {
                  		trace.severe("Unexpected error.", e);
                  	}
        		}
        	}
        	throw new SNMPGetException(0,SNMPRequestException.FAILED);
        }
    }

    public SNMPSequence processGetNextRequest(SNMPPDU snmppdu, int version,String communityName) throws SNMPGetException {
    	snmpInPktsCount++;
    	//System.out.println("Version = "+version);
        try {
            if ((SNMP_COMMUNITY.equalsIgnoreCase(communityName))&&(snmpVersion==version)) {
                return muxReqGetSetGetnext(snmppdu);
            }else{
            	if(snmpVersion!=version){
            		snmpInBadVersionsCount++;
            	}
            	if (!SNMP_COMMUNITY.equalsIgnoreCase(communityName)){
            		snmpInBadCommunityNamesCount++;
            		
            		if(snmpEnableAuthenTrapsValue==1){
            			try {
                      		sendSNMPTrap(new SNMPv2TrapPDU(new SNMPObjectIdentifier(authenticationFailureOID),getSysUptime()));
                      		throw new SNMPGetException(0,SNMPRequestException.FAILED);
                      	} catch (SNMPBadValueException e) {
                      		trace.severe("Unexpected error.", e);
                      	}
            		}
            	}
            	throw new SNMPGetException(0,SNMPRequestException.FAILED);
            }
        } catch (SNMPSetException e) {  // this exception can not happen since Set is never called from this context.
            trace.log(Level.FINER,"Exception during SNMPSetRequest",e);
            throw new SNMPGetException(0,SNMPRequestException.FAILED);
        }
    }


    // ============== Utility methods to handling SNMP request
    /**
     * Given a PDU, it is decided how to handle the request.  The appropriate method is then invoked on the deligator
     * for each SNMP OID in the PDU (while loop).  The result is in the format expected by SNMPJava API so is passed
     * straight through.
     */
    private SNMPSequence muxReqGetSetGetnext(SNMPPDU snmppdu) throws SNMPGetException, SNMPSetException {

        SNMPVarBindList snmpRet = new SNMPVarBindList();

        switch (snmppdu.getPDUType()) {
            case SNMPBERCodec.SNMPGETREQUEST:
                trace.fine("SNMPGETREQUEST. PDU: "+ snmppdu);

                SNMPSequence snmpIn = snmppdu.getVarBindList();  // get SNMP
                for (int i = 0; i < snmpIn.size(); i++) {
                    SNMPSequence oidPair = (SNMPSequence) snmpIn.getSNMPObjectAt(i);
                    SNMPObjectIdentifier oid = (SNMPObjectIdentifier) oidPair.getSNMPObjectAt(0);

                    try {
                       if(!oid.toString().startsWith(SNMPTreeNexus.finalOID)) {
                            SNMPObject retValue = snmpDeligator.getRequest(oid);

                            // only add if we were able to resolve and handle the OID.
                            if (retValue != null) {
                                snmpRet.addSNMPObject(new SNMPVariablePair(oid, retValue));
                            }
                       }
                    } catch (SNMPBadValueException e) {
                        trace.severe("Could not convert the return value: ", e);
                    }
                }
                break;
            case SNMPBERCodec.SNMPGETNEXTREQUEST:
                trace.fine("SNMPGETNEXTREQUEST. PDU: "+ snmppdu);

                snmpIn = snmppdu.getVarBindList();  // get SNMP
                //System.out.println("The size of the var bing list "+ snmppdu.size());
                for (int i = 0; i < snmpIn.size(); i++) {
                    SNMPSequence oidPair = (SNMPSequence) snmpIn.getSNMPObjectAt(i);
                    SNMPObjectIdentifier oid = (SNMPObjectIdentifier) oidPair.getSNMPObjectAt(0);
                    ///System.out.println("The oid is: "+oid.toString());


                    try {
                        if(!oid.toString().startsWith(SNMPTreeNexus.finalOID)){
                        	
                            SNMPVariablePair pair = snmpDeligator.getNextRequest(oid);

                            // only add if we were able to resolve and handle the OID.
                            if (pair != null) {
                                snmpRet.addSNMPObject(new SNMPVariablePair(oid, pair));
                            }
                        }
                    } catch (SNMPBadValueException e) {
                        trace.severe("Could not convert the return value: ", e);
                    }
                }
                break;
            case SNMPBERCodec.SNMPSETREQUEST:
                trace.fine("SNMPSETREQUEST. PDU: "+ snmppdu);

                snmpIn = snmppdu.getVarBindList();  // get SNMP
                SNMPVarBindList retValue = snmpDeligator.setRequest(snmpIn);

                // only add if we were able to resolve and handle the OID.
                if (retValue != null) {
                    snmpRet = retValue;
                }
                break;
            case SNMPBERCodec.SNMPGETRESPONSE:
                trace.fine("SNMPGETRESPONSE. PDU: "+ snmppdu);
                snmpInASNParseErrsCount++;
                break;
            case SNMPBERCodec.SNMPTRAP:
            	snmpInASNParseErrsCount++;
                trace.fine("SNMPTRAP. PDU: "+ snmppdu);
                break;
            default:
                trace.fine("unknown. PDU: "+ snmppdu);
                snmpInASNParseErrsCount++;
                break;
        }

        trace.finer("Result PDU: " + snmpRet);

        return snmpRet;
    }


    // ============= Implemented abstract methods from the ManagedServiceImpl
    public boolean initialise(SRTE srte, Properties properties) {
        if (super.initialise(srte, properties)) {
            trace.entering(getClass(), "initialise");

            try {
                nexus = SNMPInteractionToNexusImpl.getInstance(srte, properties);

                configure();

                // Create the SNMP deligator and then register all the handlers we know.
                snmpDeligator = new SNMPMainDeligator();
                snmpDeligator.addRequestHandler(new AgentcfgRH(nexus));
                snmpDeligator.addRequestHandler(new AlarmHistoryRH(nexus));
                snmpDeligator.addRequestHandler(new ActContEventsRH(nexus));
                snmpDeligator.addRequestHandler(new GroupDB_RH(nexus));
                snmpDeligator.addRequestHandler(new GroupGeneralRH(nexus));
                snmpDeligator.addRequestHandler(new GroupLicRH(nexus));
                snmpDeligator.addRequestHandler(new GroupMediaRH(nexus));
                snmpDeligator.addRequestHandler(new GroupRecIP_RH(nexus));
                snmpDeligator.addRequestHandler(new GroupRecOrionRH(nexus));
                snmpDeligator.addRequestHandler(new GroupRC_RH(nexus));
                snmpDeligator.addRequestHandler(new GroupStorageRH(nexus));
                snmpDeligator.addRequestHandler(new EventNotificationsRH(nexus, this));
                
                snmpDeligator.addRequestHandler(new GroupMgmtGroupRH(nexus,this));

                // Create a SNMP agent and register us as a listner.
                snmpAgent = new SNMPv1AgentInterface(snmpVersion, SNMP_CLIENT_PORT);

                snmpAgent.addRequestListener(this);
                startupTime = new Long(System.currentTimeMillis());

                logServiceLoadOKAuditEvent();

                return true;
            } catch (Exception e) {
                trace.severe("Failed to initialise: ", e);
                logServiceLoadFailAlarm();
            }
        }

        return false;
    }

    public boolean activate() {
        trace.entering(getClass(), "activate");
        try {
            // Potential problem here, ServerCore Creates and starts a new ModelUpdater ... do
            //  we want this option, if not then how do we start at least one?
            nexus.activate();

            snmpDeligator.activate();

            snmpAgent.startReceiving();

            // Track changes to the SystemProperty Table
            try {
                versionTracker = (VersionTracker) nexus.getSrte().getNamedService(VersionTracker.IID_VersionTracker);
            } catch (SRTEException e) {
                trace.severe("Could not retrieve the VersionTracker from the SRTE: ", e);
            }

            versionTracker.registerObserver(this, SYSTEM_PROPERTY_TABLE_NAME);
           



            return true;
        } catch (Exception ex) {
            trace.severe("activate failed: ", ex);
        }

        return false;
    }

    public void deactivate() {
        trace.entering(getClass(), "deactivate");

        versionTracker.unregisterObserver(this);

        snmpDeligator.deactivate();

        nexus.deactivate();
    }

    public void destroy() {
        trace.entering(getClass(), "destroy");
        nexus.destroy();
    }

    /**
     * Given a PDU, a trap is sent to all configured IP's.  The trap sending port is not configurable (limitation
     * to the SNMPJava API).
     *
     * @param pdu  The SNMPTrapPDU to send.
     */
    public void sendSNMPTrap(Object pdu) {
    	
        for (Iterator itt = agentsForTrap.iterator(); itt.hasNext();) {
            String agent = (String) itt.next();
            try {
            	SNMPTrapSenderInterface trapSender = new SNMPTrapSenderInterface();
            	if(snmpVersion==SNMPv1){
            		trapSender.sendTrap(InetAddress.getByName(agent), SNMP_COMMUNITY, (SNMPv1TrapPDU)pdu);
            	}else{
            		trapSender.sendTrap(InetAddress.getByName(agent), SNMP_COMMUNITY, (SNMPv2TrapPDU)pdu);
            	}
            } catch (UnknownHostException e) {
                trace.severe("Problem resolving SNMP trap end-point: " + agent, e);
            } catch (IOException e) {
                trace.severe("Problem sending SNMP trap to end-point: " + agent, e);
            }
        }
    }

    /**
     * Callback method to the observer, a call to this method indicates that some values has changed in the
     * SystemProperty table.  This will force a reconfigure of this class.<br/>
     * This method is not thread-save for this implementation.
     */
    public void versionsChanged(VersionTracker tracker, Set items) {
        for (Iterator i = items.iterator(); i.hasNext();) {
            String s = (String) i.next();
            if ((SYSTEM_PROPERTY_TABLE_NAME).equals(s)) {
                configure();
            }
        }
    }

    /**
     * Read values for the port, IP's, and community from nexus (SystemProperty table).
     */
    private void configure() {
        String community = nexus.getNexusConfigurationProperty("SNMPCommunity");
        if (community != null) {
            SNMP_COMMUNITY = community;
        }

        String cliPort = nexus.getNexusConfigurationProperty("SNMPClientPort");
        if (cliPort != null) {
            SNMP_CLIENT_PORT = Integer.parseInt(cliPort);
        }

        String SNMPAgentIPs = nexus.getNexusConfigurationProperty("SNMPAgentIPs");
        if (SNMPAgentIPs != null) {
            agentsForTrap.clear();
            StringTokenizer tokens = new StringTokenizer(SNMPAgentIPs, ",");

            while (tokens.hasMoreElements()) {
                String token = (String) tokens.nextElement();
                agentsForTrap.add(token);
            }
        }
    }
    public SNMPTimeTicks getSysUptime(){
    	if(startupTime ==null){
    		startupTime=new Long(System.currentTimeMillis());
    	}
    	return new SNMPTimeTicks((System.currentTimeMillis()-startupTime.longValue())/10);
    }

	public int getSnmpInPktsCount() {
		return snmpInPktsCount;
	}

	public int getSnmpInBadCommunityNamesCount() {
		return snmpInBadCommunityNamesCount;
	}

	public int getSnmpInASNParseErrsCount() {
		return snmpInASNParseErrsCount;
	}

	public int getSnmpEnableAuthenTrapsValue() {
		return snmpEnableAuthenTrapsValue;
	}

	public void setSnmpEnableAuthenTrapsValue(int snmpEnableAuthenTrapsValue) {
		this.snmpEnableAuthenTrapsValue = snmpEnableAuthenTrapsValue;
	}

	public int getSnmpInBadVersionsCount() {
		return snmpInBadVersionsCount;
	}

	public int getSnmpVersion() {
		return snmpVersion;
	}
    
   

}