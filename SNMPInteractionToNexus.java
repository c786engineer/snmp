package com.dvsoft.nexus.service.systemmanagement.snmp;

import com.dvsoft.nexus.service.SRTE;

import java.util.Vector;
import java.util.List;

import snmp.SNMPTimeTicks;

/**
 * All interactions that the SNMP module has with the Nexus specifics should go through this interface.  At this stage
 * the only other interactions with nexus is the VersionTracker used to detect changes to the configuration data.
 *
 * @see com.dvsoft.nexus.versiontracker.VersionTracker
 * @see SNMPInteractionToNexusImpl
 */
public interface SNMPInteractionToNexus {

    // Lifecylce methods... don't need them all...
    public void activate();
    public void deactivate();
    public void destroy();

    // ====== Get the current alarms...
    /** Gets the Cont Alarms from the HealthModel using the ServerCore. */
    public Vector getContinuousAlarms();
    /** Gets all the alarms events from the HealthModel using the ServerCore. */
    public Vector getAlarmEvents();

    // ====== Methods for specific event URI's
    /** Given a event URI, find the SNMP event group this event belongs to. */
    public String getSnmpEventGroup(String eventURI);
    /** Given a event URI, find the standard event group this event belongs to. */
    public String getEventGroup(String eventURI);
    /** Retrieve all the alarms events for the specified SNMP event group. */
    public Vector getAlarmEvents(String eventURI);
    /** Retrieve the lexographical name for the event given the event URI. */
    public String getName(String eventURI, String[] param);
    /** Retrieve the lexographical message for the event given the event URI with the parameters substituted. */
    public String getMessage(String eventURI, String[] param);

    // ====== retrieve configuration items from nexus
    /** retrieve configuration items from nexus. */
    public String getNexusConfigurationProperty(String propertyName);
    /** Retrieve the URI's of all the events that SNMP should monitor (send traps on new occurances). */
    public List retrieveSnmpTrapUriS();

    // ====== Use these to detect and retrieve new entries in the events vector
    /** The index for the last event in the model at the time of invoking this method.  Changes in the index indicates
     * new events. */
    public long getMaxAlarmEventID();
    /** Retrieve all events that occurred from an index. */
    public Vector getAlarmEventsSince(long previousMaxAlarmEventID);

    /** Used to get services. */
    SRTE getSrte();
    
   
}
