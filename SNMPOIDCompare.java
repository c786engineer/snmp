package com.dvsoft.nexus.service.systemmanagement.snmp;

import java.util.Comparator;

/**
 * Rather simple oid compare algorithm, sorts OID by node values (i.e. not as a string 1.10.1 > 1.1.2.2)
 */
public class SNMPOIDCompare implements Comparator {
    public int compare(Object o1, Object o2) {
        if (o1 != null && o2 != null) {
        	long[] oid1 = (long[]) ((TreeEntry) o1).getOid().getValue();
            long[] oid2 = (long[]) ((TreeEntry) o2).getOid().getValue();

            for (int i = 0; i < oid1.length; i++) {
                if (i < oid2.length) {
                    if (oid1[i] != oid2[i]) {
                        return (int)(oid1[i] - oid2[i]);  // we've found a node that did not match.  Calculate the result.
                    }
                } else {
                    // since oid1 is longer than oid2, and since we have not found any mismatch to this point, we
                    //  know oid1 should be below oid2 (eg. 1.2.3.4.5 and 1.2.3.4, the first is a subtree of the second)
                    return 1;
                }
            }

            // equal so far, but lets check that the second was'nt longer (i.e. a sub-node).
            if (oid1.length < oid2.length) {
                return -1;
            }
        }

        return 0;
    }

}
