/**
 * Revision History:
 * 
 * 2011-06-01	NP	Modified translateAlarmToTrap() to include 'raised' alarm action.
 */
package com.dvsoft.nexus.service.systemmanagement.snmp.requesthandlers;

import snmp.*;

import com.dvsoft.trace.Trace;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPInteractionToNexus;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPMonitor;
import com.dvsoft.nexus.systemmanagement.AlarmEvent;
import com.dvsoft.nexus.event.AlarmEventAction;
import com.dvsoft.nexus.event.AlarmEventType;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.math.BigInteger;

/**
 */
public class EventNotificationsRH extends SNMPRequestHandler implements Runnable {
    private static Trace trace = Trace.get(AgentcfgRH.class);
    private static String baseOID = "1.3.6.1.4.1.1484.1.1.2.4"; // iso.org.dod.internet.private.enterprises.datavoice.system.nexusLibra.alarms.eventNotifications
    private static final int OID_COLUMN_INDEX_URI = 13;
    private static final int OID_ROW_INDEX_URI = 14;
    private static final int OID_COLUMN_INDEX_FILTERED = 14;
    private static final int OID_ROW_INDEX_FILTERED = 15;

    private Vector alarms;

    private List uriList = new ArrayList();

    private boolean stop = false;
    private Thread thread;

    private long prevMaxAlarmEventID;
    private SNMPMonitor main;
    private int pollInterval = 10;
    private int maxRows = 100;

    long loadTimeMS = System.currentTimeMillis();
    private long previousTime;
    private int cacheLifeSec;

  public EventNotificationsRH(SNMPInteractionToNexus nexus, SNMPMonitor monitor) {
        super(nexus);

        main = monitor;

        super.addOIDRange(baseOID + ".*");    // .iso.org.dod.internet.private.enterprises.datafusion.propriety.nexus.alarms.eventNotifications
    }

    public void activate() {
        super.activate();

        prevMaxAlarmEventID = nexus.getMaxAlarmEventID();

        uriList = nexus.retrieveSnmpTrapUriS();

        thread = new Thread(this, "SNMP new alarm traps");
        thread.start();
    }

    public void deactivate() {
        super.deactivate();

        stop = true;
        thread.interrupt();
    }

    /**
     */
    protected SNMPObjectIdentifier getNextOIDIfKnown(SNMPObjectIdentifier oid) {
        try {
            if (oid.toString().startsWith(baseOID + ".2.1")) {
            	return calculateNextOIDForTable(oid, baseOID + ".2", 12, 3, uriList.size());
            } else if (oid.toString().startsWith(baseOID + ".1.3.1")) {
                refreshAlarms();
                return calculateNextOIDForTable(oid, baseOID + ".1.3", 13, 16, maxRows < alarms.size() ? maxRows : alarms.size());
            }
        } catch (SNMPBadValueException e) {
            trace.severe("Error converting a String to a OID: ", e);
        }
        return null;  // Typically look in SNMPTreeNexus
    }

    public SNMPObject getValueForOID(SNMPObjectIdentifier oid) {
        if (oid.toString().startsWith(baseOID + ".2.1")) {  // Are we looking at the table?
            int row = 0, column;

            if ((column = extractColumn(OID_COLUMN_INDEX_URI, oid)) > 0) {
                row = extractRow(OID_ROW_INDEX_URI, oid) - 1;  // zero returned if not existant, 1 for first row (so have to -1 to zero-base it)

                if (row >= 0) { // row of -1 = reached the end...
                    String url = (String) (uriList.toArray()[row]);

                    switch (column) {
                        case 1: // monitoredURL
                            return new SNMPCounter32(row);
                        case 2: // trapUrlRowStatus
                            return new SNMPOctetString(url);
                        case 3: // trapUrlRowStatus
                            return new SNMPCounter32(0);
                        default:
                            return new SNMPOctetString("unknown column...");
                    }
                }
            }
        } else if (oid.toString().startsWith(baseOID + ".1.1.0")) {
            return new SNMPInteger(maxRows);
        } else if (oid.toString().startsWith(baseOID + ".1.2.0")) {
            refreshAlarms();
            return new SNMPInteger(alarms.size());
        } else if (oid.toString().startsWith(baseOID + ".1.3.1")) {
            int row = 0, column;

            if ((column = extractColumn(OID_COLUMN_INDEX_FILTERED, oid)) > 0) {
                row = extractRow(OID_ROW_INDEX_FILTERED, oid) - 1;  // zero returned if not existant.

                if (row >= 0) {  // row of -1 = reached the end...
                    refreshAlarms();
                    AlarmEvent alarm = (AlarmEvent) alarms.get(row);

                    switch (column) {
                        case 1: // alleEventIndex (typically not requested)
                            return new SNMPInteger(row);
                        case 2: // alleEventIndex (typically not requested)
                            return new SNMPCounter32(alarm.getID());
                        case 3: // alleEventUrl
                            return new SNMPOctetString(alarm.getEventType().getURI());
                        case 4: // alleLastChangeDateTime
                            return new SNMPOctetString(SNMPRequestHandler.dateTimeFormatter.format(alarm.getDateTime()));
                        case 5: // alleEventGroup
                          String group = nexus.getEventGroup(alarm.getEventType().getURI());
                          return new SNMPOctetString(group != null ? group : "unknown URI");
                        case 6: // alleName
                            String name = nexus.getName(alarm.getEventType().getURI(), alarm.getParameters());
                            return new SNMPOctetString(name != null ? name : "unknown URI");
                        case 7: // alleDescription
                            String message = nexus.getMessage(alarm.getEventType().getURI(), alarm.getParameters());
                            return new SNMPOctetString(message != null ? message : "unknown URI");
                        case 8: // alleStatus
                        	return new SNMPOctetString(alarm.getAction().toString());
//                            if (alarm.getAction().equals(AlarmEventAction.FIRED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPCounter32(1);
//                            } else if (alarm.getAction().equals(AlarmEventAction.UPDATED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPCounter32(2);
//                            } else if (alarm.getAction().equals(AlarmEventAction.CLEARED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPCounter32(3);
//                            } else if (alarm.getAction().equals(AlarmEventAction.FIRED)) {
//                            	return new SNMPOctetString(alarm.getAction().toString());
//                            	//return new SNMPCounter32(4);
//                            } else {
//                            	return new SNMPOctetString("unknown");
//                            	//return new SNMPCounter32(5);  // unknown
//                            }
                        case 9: // alleContinues
                            if (alarm.getEventType() instanceof AlarmEventType) {
                                if (((AlarmEventType) alarm.getEventType()).isContinuous()) {
                                    return new SNMPCounter32(1);
                                } else {
                                    return new SNMPCounter32(2);
                                }
                            }
                        case 10: // alleSourceHost
                            return new SNMPOctetString(alarm.getSourceLocation().getHostName());
                        case 11: // alleSourceHostType
                            return new SNMPOctetString(alarm.getSourceLocation().getHostType());
                        case 12: // alleSourceProcess
                            return new SNMPOctetString(alarm.getSourceLocation().getProcessName());
                        case 13: // alleSourceProcessType
                            return new SNMPOctetString(alarm.getSourceLocation().getProcessType());
                        case 14: // alleSourceService
                            return new SNMPOctetString(alarm.getSourceLocation().getServiceName());
                        case 15: // alleSourceServiceType
                            return new SNMPOctetString(alarm.getSourceLocation().getServiceType());
                        case 16: // alleRowStatus
                            return new SNMPInteger(0);
                        default:
                            return new SNMPOctetString("unknown column...");
                    }
                }
            }
        }
        return null;
    }

    private void refreshAlarms() {
        long time = System.currentTimeMillis();

        // since these filtered models are relatively expensive to build, only get if it's older than 10sec
        if (time > previousTime + (cacheLifeSec*1000)) {
          Vector allAlarms = nexus.getAlarmEvents();
          Vector filteredAlarms = new Vector();

          uriList = nexus.retrieveSnmpTrapUriS();

          for (int i = 0; i < allAlarms.size(); i++) {
              AlarmEvent event = (AlarmEvent) allAlarms.elementAt(i);

              String uri = event.getEventType().getURI();

              for (int j = 0; j < uriList.size(); j++) {
                  String uriAllowed = (String) uriList.get(j);
                  if (uri.equalsIgnoreCase(uriAllowed)) {
                      filteredAlarms.add(event);
                  }
              }
          }

          alarms = filteredAlarms;
          previousTime = time;
        }
    }

    public SNMPVarBindList setValueForOID(SNMPSequence pdu) throws SNMPSetException {
        for (int i = 0; i < pdu.size(); i++) {
            SNMPObjectIdentifier oid = (SNMPObjectIdentifier)((SNMPSequence)pdu.getSNMPObjectAt(i)).getSNMPObjectAt(0);
            SNMPObject value = ((SNMPSequence)pdu.getSNMPObjectAt(i)).getSNMPObjectAt(1);
            if (oid.toString().startsWith(baseOID + ".2.1.2")) { // URL
                uriList.add(value.toString());
            } else if (oid.toString().startsWith(baseOID + ".2.1.3") && value.toString().equals("6")) {  // delete a column requested
                int[] nodes = (int[])oid.getValue();
                int row = nodes[nodes.length - 1];

                uriList.remove(row - 1);
            } else if (oid.toString().startsWith(baseOID + ".1.1.0")) {
                maxRows = ((BigInteger) value.getValue()).intValue();
            }
        }
        throw new SNMPSetException("Setting is not supported for this OID", 0, SNMPRequestException.VALUE_READ_ONLY);
    }

    protected void refreshSystemProperties() {
        pollInterval = Integer.parseInt(nexus.getNexusConfigurationProperty("SNMPTrapPollInterval"));
        cacheLifeSec = pollInterval;

        maxRows = Integer.parseInt(nexus.getNexusConfigurationProperty("SNMPMaxRowCount"));
    }

    // From Runnable interface.
    public void run() {
        while (!stop) {
            try {
                long max = nexus.getMaxAlarmEventID();

                if (prevMaxAlarmEventID < max) {
                    trace.finer("New alarms detected!");

                    Vector newAlams = nexus.getAlarmEventsSince(prevMaxAlarmEventID);

                    // Don't hit the DB unless there are new events.
                    if (newAlams.size() > 0) {
                        uriList = nexus.retrieveSnmpTrapUriS();
                    }

                    // find alarms that we know we should send as traps
                    for (int i = 0; i < newAlams.size(); i++) {
                        AlarmEvent ae = (AlarmEvent) newAlams.elementAt(i);
                        String uri = ae.getEventType().getURI();

                        for (int j = 0; j < uriList.size(); j++) {
                            String filteredUri = (String) uriList.get(j);

                            if (filteredUri.equals(uri)) {
                            	Object pdu = translateAlarmToTrap(ae);
                            	
                            	
                                if (pdu != null) {
                                	main.sendSNMPTrap(pdu);
                                }
                            }
                        }
                    }

                    prevMaxAlarmEventID = max;
                }

                Thread.sleep(pollInterval*1000);
            } catch (InterruptedException e) {
                // This is expected - at shutdown.
            } catch (SNMPBadValueException e) {
                prevMaxAlarmEventID = nexus.getMaxAlarmEventID();  // don't want to go into a endless loop, with no sleep()...
                trace.severe("Unexpected error.", e);
            }
        }
    }

    /**
     * Translate an AlarmEvent into a SNMP trap PDU, this PDU can then be sent to the appropriate SNMP Trap monitors.
     * @param alarm Alarm to convert
     * @return the SNMP PDU that can be sent as is (could contain the dest IP.
     */
    private Object translateAlarmToTrap(AlarmEvent alarm) throws SNMPBadValueException {
        try {
            long ticks = (System.currentTimeMillis() - loadTimeMS)/10;
            String thisIP = InetAddress.getLocalHost().getHostAddress();
            SNMPSequence message = new SNMPSequence();
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.2"), new SNMPCounter32(alarm.getID())));
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.3"), new SNMPOctetString(alarm.getEventType().getURI())));
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.4"), new SNMPOctetString(SNMPRequestHandler.dateTimeFormatter.format(alarm.getDateTime()))));
            String group = nexus.getEventGroup(alarm.getEventType().getURI());
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.5"), new SNMPOctetString(group != null ? group : "unknown URI")));
            String name = nexus.getName(alarm.getEventType().getURI(), alarm.getParameters());
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.6"), new SNMPOctetString(name != null ? name : "unknown URI")));
            String message1 = nexus.getMessage(alarm.getEventType().getURI(), alarm.getParameters());
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.7"), new SNMPOctetString(message1 != null ? message1 : "unknown URI")));

            if (alarm.getAction().equals(AlarmEventAction.FIRED)) {
                message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.8"), new SNMPOctetString(alarm.getAction().toString())));
            } else if (alarm.getAction().equals(AlarmEventAction.UPDATED)) {
                message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.8"), new SNMPOctetString(alarm.getAction().toString())));
            } else if (alarm.getAction().equals(AlarmEventAction.CLEARED)) {
                message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.8"), new SNMPOctetString(alarm.getAction().toString())));
            } else if (alarm.getAction().equals(AlarmEventAction.RAISED)) {
                message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.8"), new SNMPOctetString(alarm.getAction().toString())));
            } else {
                message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.8"), new SNMPOctetString("unknown")));
            }

            if (alarm.getEventType() instanceof AlarmEventType) {
                if (((AlarmEventType) alarm.getEventType()).isContinuous()) {
                    message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.9"), new SNMPCounter32(1)));
                } else {
                    message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.9"), new SNMPCounter32(2)));
                }
            }

            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.10"), new SNMPOctetString(alarm.getSourceLocation().getHostName())));
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.11"), new SNMPOctetString(alarm.getSourceLocation().getHostType())));
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.12"), new SNMPOctetString(alarm.getSourceLocation().getProcessName())));
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.13"), new SNMPOctetString(alarm.getSourceLocation().getProcessType())));
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.14"), new SNMPOctetString(alarm.getSourceLocation().getServiceName())));
            message.addSNMPObject(new SNMPVariablePair(new SNMPObjectIdentifier(baseOID + ".1.3.1.15"), new SNMPOctetString(alarm.getSourceLocation().getServiceType())));
            
            
            //Now send the trap with the correct trap number defined in Datavoice.mib
            if (alarm.getEventType() instanceof AlarmEventType) {
            	
            	if (((AlarmEventType) alarm.getEventType()).getCategory().compareTo("Critical Alarm")==0) {
            		if(main.getSnmpVersion()==SNMPMonitor.SNMPv1){
            			return new SNMPv1TrapPDU(new SNMPObjectIdentifier(baseOID+".1" ), new SNMPIPAddress(thisIP), 6, 1, new SNMPTimeTicks(ticks), message);
            		}else{
            			return new SNMPv2TrapPDU(main.getSysUptime(),new SNMPObjectIdentifier(baseOID+".3" ), message);
            		}
            	}
            	if (((AlarmEventType) alarm.getEventType()).getCategory().compareTo("Warning Alarm")==0) {
            		if(main.getSnmpVersion()==SNMPMonitor.SNMPv1){
            			return new SNMPv1TrapPDU(new SNMPObjectIdentifier(baseOID+".1" ), new SNMPIPAddress(thisIP), 6, 2, new SNMPTimeTicks(ticks), message);
            		}else{
            			return new SNMPv2TrapPDU(main.getSysUptime(),new SNMPObjectIdentifier(baseOID+".4" ), message);
            		}
            	}
            	if (((AlarmEventType) alarm.getEventType()).getCategory().compareTo("Info Alarm")==0) {
            		if(main.getSnmpVersion()==SNMPMonitor.SNMPv1){
            			return new SNMPv1TrapPDU(new SNMPObjectIdentifier(baseOID+".1" ), new SNMPIPAddress(thisIP), 6, 3, new SNMPTimeTicks(ticks), message);
            		}else{
            			return new SNMPv2TrapPDU(main.getSysUptime(),new SNMPObjectIdentifier(baseOID+".5" ), message);
            		}
            	}
            	
            }
            if(main.getSnmpVersion()==SNMPMonitor.SNMPv1){
    			return new SNMPv1TrapPDU(new SNMPObjectIdentifier(baseOID+".1" ), new SNMPIPAddress(thisIP), 6, 3, new SNMPTimeTicks(ticks), message);
            }else{
            	return new SNMPv2TrapPDU(main.getSysUptime(),new SNMPObjectIdentifier(baseOID+".3" ), message);
            }
        } catch (UnknownHostException e) {
            trace.severe("Could not resolve IP for the local interface: ", e);
        }
        return null;
    }
}
