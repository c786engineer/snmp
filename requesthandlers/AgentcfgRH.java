package com.dvsoft.nexus.service.systemmanagement.snmp.requesthandlers;

import snmp.*;
import com.dvsoft.trace.Trace;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPInteractionToNexus;

/**
 */
public class AgentcfgRH extends SNMPRequestHandler {
  private static final String baseOID = "1.3.6.1.4.1.1484.1.1";
//    private static Trace trace = Trace.get(AgentcfgRH.class);

    public AgentcfgRH(SNMPInteractionToNexus nexus) {
        super(nexus);
        //super.addOIDRange(baseOID+".*");    // .iso.org.dod.internet.private.enterprises.datafusion.propriety.nexus.agentcfg
        super.addOIDRange(baseOID+".1.*");
        super.addOIDRange(baseOID+".2");
        super.addOIDRange(baseOID+".2.3");
        super.addOIDRange("1.3.6.1.4.1.1484");
        super.addOIDRange("1.3.6.1.4.1.1484.1");
        super.addOIDRange("1.3.6.1.4.1.1484.1.1");
    }

    /**
     * @return The default implementation (MIB tree) will do fine, so just return null.
     * @see com.dvsoft.nexus.service.systemmanagement.snmp.SNMPTreeNexus
     */
    protected SNMPObjectIdentifier getNextOIDIfKnown(SNMPObjectIdentifier oid) {
        return null;
    }

    public SNMPObject getValueForOID(SNMPObjectIdentifier oid) {
    	if (oid.toString().startsWith(baseOID + ".1.1")) {
      	 PackageVersion p = new PackageVersion();
      	 return new SNMPOctetString(p.getMajor()+"."+p.getMinor());
      } else if (oid.toString().startsWith(baseOID + ".1.2")) {
      	return new SNMPOctetString(System.getProperty("os.name"));
      }

      return null;
    }

    public SNMPVarBindList setValueForOID(SNMPSequence pdu) throws SNMPSetException {
        throw new SNMPSetException("Setting is not supported for this OID", 0, SNMPRequestException.VALUE_READ_ONLY);
    }

    protected void refreshSystemProperties() {
    }
}
