package com.dvsoft.nexus.service.systemmanagement.snmp.requesthandlers;

import java.util.Vector;

import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPInteractionToNexus;
import com.dvsoft.trace.Trace;

/**
 */
public class GroupLicRH extends SNMPEventGroupRequestHandler {
    // state or constants specific to event group beeing implemented.
    protected static Trace trace = Trace.get(GroupLicRH.class);
    private static String baseOID = "1.3.6.1.4.1.1484.1.1.2.3.3";

    private Vector alarms;
    private int maxRows = 100;
    private long previousTime = 0;

    public GroupLicRH(SNMPInteractionToNexus nexus) {
        super(nexus);
    }

    protected String getBaseOID() {
        return baseOID;
    }

    protected String getEventGroupImplemented() {
        return SNMP_EVENT_GROUP_LICENSING;
    }


    protected Vector getAlarms() {
        return alarms;
    }

    protected void setAlarms(Vector alarms) {
        this.alarms = alarms;
    }

    protected int getMaxRows() {
        return maxRows;
    }

    protected void setMaxRows(int maxRows) {
        this.maxRows = maxRows;
    }

    protected long getPreviousTime() {
        return previousTime;
    }

    protected void setPreviousTime(long previousTime) {
        this.previousTime = previousTime;
    }

}
