/**
 * Copyright	: Copyright (C) 2006, DataVoice PTY. (LTD).
 *              : All rights reserved
 *
 * Program 		: TreeEntry.java
 * Program by	: hventer
 * Created 		: 2006/07/14
 * Description	: See JavaDoc comments (below)
 *
 * Revisions	: 
 */
package com.dvsoft.nexus.service.systemmanagement.snmp;

import snmp.SNMPObjectIdentifier;

/**
 * @author hventer
 *
 */
public class TreeEntry {
	
    	private SNMPObjectIdentifier oid;
		private boolean returnable;

		TreeEntry(SNMPObjectIdentifier oid, boolean returnable){
    		this.oid=oid;
    		this.returnable=returnable;
    	}

		public SNMPObjectIdentifier getOid() {
			return oid;
		}

		public boolean isReturnable() {
			return returnable;
		}
		
		
    

}
