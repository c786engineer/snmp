package com.dvsoft.nexus.service.systemmanagement.snmp;

import java.util.Iterator;
import java.util.TreeSet;

import snmp.SNMPBadValueException;
import snmp.SNMPObjectIdentifier;

import com.dvsoft.trace.Trace;

/**
 * Simple tree for linear transversal.  Use purely for getting the next node in the MIB.  This Class is typically used
 * by a SNMPRequestHandler when that handler no longer knows what the next node is (e.g. the end of a table has been
 * reached and the handler has no knowladge of the MIB outside it's own OID range).
 * <br/><br/>
 * The tree is loaded once only per classloader.<br/>
 * Rules for MIB tree:
 * <ol>
 *  <li>Nodes will be ordered when instered into the in-memory tree.</li>
 *  <li>Leave nodes (should have values associated) always end in <code>.0</code> .</li>
 *  <li>Tables are stored upto the Entry level, these will typically be iterated through by the appropriate handler.</li>
 * </ol>
 * <br/>
 * If more than one SNMP tree is to be supported, the <code>getNextOID</code> method may be better fitted
 * in an interface...<br/>
 * @see com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler
 * 
 * 2005-10-26 HVenter updated the class to reflect the complete NexusSerber.mib MIB tree
 */
public class SNMPTreeNexus {
    private TreeSet tree = new TreeSet(new SNMPOIDCompare());
    private static SNMPTreeNexus instance;
    private static Trace trace = Trace.get(SNMPTreeNexus.class);
    
    public static String finalOID = "1.3.6.1.4.1.1484.1.1.2.4.3" ;

    private SNMPTreeNexus() {
    	 
        loadTree();
    }

    /**
     * Get hold of this class.
     */
    public static SNMPTreeNexus getInstance() {
        if (instance == null) {
            instance = new SNMPTreeNexus();
        }

        return instance;
    }
    
    

    /**
     */
    private void loadTree() {
        try {
            //tree.add(new SNMPObjectIdentifier("1.0"));                                // .iso
            //tree.add(new SNMPObjectIdentifier("1.3"));                              // .iso.org
            //tree.add(new SNMPObjectIdentifier("1.3.6"));                            // .iso.org.dod
            //tree.add(new SNMPObjectIdentifier("1.3.6.1"));                          // .iso.org.dod.internet
            //The system group
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1"),false));                      // .iso.org.dod.internet.mgmt.mib-2.system
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.1.0"),true));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysDesc
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.2.0"),true));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysobjectID
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.3.0"),true));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysUpTime
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.4.0"),true));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysContact
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.5.0"),true));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysName
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.6.0"),true));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysLocation
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.7.0"),true));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysSevices
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.8.0"),true));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysORLastChange
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.9"),false));                    // .iso.org.dod.internet.mgmt.mib-2.system.sysORTable
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.9.1"),false));                  // .iso.org.dod.internet.mgmt.mib-2.system.sysORTable.sysORTableEntry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.9.1.1"),true));				  // .iso.org.dod.internet.mgmt.mib-2.system.sysORTable.sysORTableEntry.sysORIndex	
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.9.1.2"),true));				  // .iso.org.dod.internet.mgmt.mib-2.system.sysORTable.sysORTableEntry.sysORID	
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.9.1.3"),true));				  // .iso.org.dod.internet.mgmt.mib-2.system.sysORTable.sysORTableEntry.sysORDesc	
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.1.9.1.4"),true));				  // .iso.org.dod.internet.mgmt.mib-2.system.sysORTable.sysORTableEntry.sysORUpTime	
            
            //snmp group
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11"),false));                      // .iso.org.dod.internet.mgmt.mib-2.snmp
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11.1.0"),true));                      // .iso.org.dod.internet.mgmt.mib-2.snmp.snmpInPkts
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11.3.0"),true));                      // .iso.org.dod.internet.mgmt.mib-2.snmp.snmpInBadVersions
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11.4.0"),true));                      // .iso.org.dod.internet.mgmt.mib-2.snmp.snmpInBadCommunityNames
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11.5.0"),true));                      // .iso.org.dod.internet.mgmt.mib-2.snmp.snmpInBadCommunityUses
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11.6.0"),true));                      // .iso.org.dod.internet.mgmt.mib-2.snmp.snmpInASNParseErrs
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11.30.0"),true));                      // .iso.org.dod.internet.mgmt.mib-2.snmp.snmpEnableAuthenTraps
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11.31.0"),true));                      // .iso.org.dod.internet.mgmt.mib-2.snmp.snmpSilentDrops
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.2.1.11.32.0"),true));                      // .iso.org.dod.internet.mgmt.mib-2.snmp.snmpProxyDrops
                       
            
             //tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4"));                        // .iso.org.dod.internet.private
             //tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1"));                      // .iso.org.dod.internet.private.enterprises
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484"),false));                 // .iso.org.dod.internet.private.enterprises.dataVoice
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1"),false));               // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1"),false));             // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem

             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.1"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.agentconfig
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.1.1.0"),true));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.agentconfig.configsystemAgentVersion
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.1.2.0"),true));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.agentconfig.agentPlatform

             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2"),false));           // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.continousAlarms
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.1.0"),true));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.actContEvents.aceMaxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.actContEvents.aceTable
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.actContEvents.aceTable.aceEntry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.1.2.1.16"),true));
            
            
            // Rest of the act table is implemented in the RequestHandler

             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmHistory
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.1.0"),true));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmHistory.alhMaxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmHistory.alhTable
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmHistory.alhTable.alhEntry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.1"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.2"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.3"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.4"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.5"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.6"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.7"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.8"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.9"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.10"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.11"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.12"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.13"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.14"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.15"),true)); 
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.2.2.1.16"),true)); 
            
            // Rest of the alh table is implemented in the RequestHandler

             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.database
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.1.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.database.maxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.2.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.database.itemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.database.table
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1"),false));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.database.table.entry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.1.3.1.16"),true));
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.general
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.1.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.general.maxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.2.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.general.itemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.general.table
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1"),false));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.general.table.entry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.2.3.1.16"),true));
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.licensing
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.1.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.licensing.maxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.2.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.licensing.itemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.licensing.table
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1"),false));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.licensing.table.entry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.3.3.1.16"),true));
            
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.media
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.1.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.media.maxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.2.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.media.itemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.media.table
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1"),false));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.media.table.entry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.4.3.1.16"),true));
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderIP
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.1.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.rececorderIP.maxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.2.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderIP.itemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderIP.table
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1"),false));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderIP.table.entry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.5.3.1.16"),true));
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderOrion
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.1.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderOrion.maxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.2.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderOrion.itemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderOrion.table
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1"),false));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recorderOrion.table.entry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.6.3.1.16"),true));
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recordingControler
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.1.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recordingControler.maxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.2.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recordingControler.itemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recordingControler.table
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1"),false));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.recordingControler.table.entry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.7.3.1.16"),true));
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8"),false));       // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.storage
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.1.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.storage.maxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.2.0"),true));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.storage.itemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3"),false));     // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.storage.table
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1"),false));   // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmGroups.storage.table.entry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.3.8.3.1.16"),true));
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmNotification
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmNotification.notifiedAlarms
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.1.0"),true));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmNotification.notifiedAlarms.trapEventsMaxRows
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.2.0"),true));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmNotification.notifiedAlarms.trapEventItemCount
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmNotification.notifiedAlarms.trapEventTable
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmNotification.notifiedAlarms.trapEventTable.trapEventEntry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.3"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.4"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.5"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.6"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.7"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.8"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.9"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.10"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.11"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.12"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.13"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.14"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.15"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.1.3.1.16"),true));
            
            //The rest is handle by te Request Handler
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.2"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmNotification.alarmsToNotify
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.2.1"),false));         // .iso.org.dod.internet.private.enterprises.dataVoice.libraNexus.dvsystem.alarms.alarmNotification.alarmsToNotify.trapAlarmURLEntry
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.2.1.1"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.2.1.2"),true));
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.2.1.3"),true));
            
             tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.3"),true));
            // tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.4"));
            // tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.5"));
            //The rest is handled by the request handler
            // tree.add(new TreeEntry(new SNMPObjectIdentifier("1.3.6.1.4.1.1484.1.1.2.4.6"));
                  //Signals the end of the tree
            
            
            trace.finest("MIB Tree supported for SNMP:");
            for (Iterator iTree = tree.iterator(); iTree.hasNext();) {
                SNMPObjectIdentifier snmpOID = ((TreeEntry) iTree.next()).getOid();
                trace.finest(snmpOID.toString());
            }
        } catch (SNMPBadValueException e) {
            e.printStackTrace();
            trace.severe("Failed to initialise: ", e);
        }
    }

    /**
     * Finds the next node from the MIB tree.
     * @return the OID to follow the one requested, if the requested OID is not found null is returned.
     */
    public SNMPObjectIdentifier getNextOID(SNMPObjectIdentifier oid) {
    	SNMPObjectIdentifier ret = simpleFindNextOID(oid);

        // since we have not found a next, this is possibly a table, so strip off the row and/or col and try once more.
        String shorterOIDStr = oid.toString().substring(0, oid.toString().lastIndexOf('.'));  // remove the '.x'
        try {
            if (ret == null) {
                SNMPObjectIdentifier shorterOID = null;
                shorterOID = new SNMPObjectIdentifier(shorterOIDStr);
                ret = simpleFindNextOID(shorterOID);
            }

            if (ret == null) {
                shorterOIDStr = oid.toString().substring(0, shorterOIDStr.lastIndexOf('.'));  // remove another '.x'

                SNMPObjectIdentifier shorterOID = null;
                shorterOID = new SNMPObjectIdentifier(shorterOIDStr);
                ret = simpleFindNextOID(shorterOID);
            }
        } catch (SNMPBadValueException e) {
        	
            // return null, so do nothing
        }

        return ret;
    }

    private SNMPObjectIdentifier simpleFindNextOID(SNMPObjectIdentifier oid) {
    	for (Iterator iTree = tree.iterator(); iTree.hasNext();) {
        	TreeEntry treeEntry = (TreeEntry) iTree.next();
        	          
           if (new SNMPOIDCompare().compare(treeEntry,new TreeEntry(oid,false)) == 0) {
            	if(iTree.hasNext()){
            		TreeEntry temp= (TreeEntry) iTree.next();
            		//if(temp.isReturnable()){
            			return temp.getOid();
            		//}else{
            			//while(iTree.hasNext()){
            				//temp = (TreeEntry) iTree.next();
            				//if(temp.isReturnable()){
            					//return temp.getOid();
            				//}
            			//}
            		//}
            	}
            }
        }
        
       
       
        return null;
    }
    
    
    
    
    
}
