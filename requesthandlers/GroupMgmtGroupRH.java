/**
 * Copyright	: Copyright (C) 2006, DataVoice PTY. (LTD).
 *              : All rights reserved
 *
 * Program 		: GroupMgmtGroupRH.java
 * Program by	: hventer
 * Created 		: 2006/07/03
 * Description	: See JavaDoc comments (below)
 *
 * Revisions	: 
 */
package com.dvsoft.nexus.service.systemmanagement.snmp.requesthandlers;

import java.math.BigInteger;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import snmp.SNMPBadValueException;
import snmp.SNMPCounter32;
import snmp.SNMPGetException;
import snmp.SNMPInteger;
import snmp.SNMPObject;
import snmp.SNMPObjectIdentifier;
import snmp.SNMPOctetString;
import snmp.SNMPRequestException;
import snmp.SNMPSequence;
import snmp.SNMPSetException;
import snmp.SNMPTimeTicks;
import snmp.SNMPVarBindList;
import snmp.SNMPVariablePair;

import com.dvsoft.nexus.service.SRTE;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPInteractionToNexus;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPMonitor;
import com.dvsoft.nexus.service.systemmanagement.snmp.SNMPRequestHandler;
import com.dvsoft.trace.Trace;

/**
 * @author hventer
 * A class that implements all that is needed for the mgmt group in SNMPv2
 * All oids in this class needed for snmp complience
 */
public class GroupMgmtGroupRH extends SNMPRequestHandler {
	 private static Trace trace = Trace.get(GroupMgmtGroupRH.class);
	
	private static final String baseOID = "1.3.6.1.2";
	private static final int OID_COLUMN_INDEX_URI = 9;
	private static final int OID_ROW_INDEX_URI = 10;
	
	//A vector containg the sysOrEntrys for the sysOrTable in the system group
	private static final Vector sysOREntries=new Vector();	
	private SNMPInteractionToNexus nexus;
	private SRTE srte;
	private String sysContact = "me@me.com";
	private String sysName = "";
	private String sysLocation = "";
	private SNMPTimeTicks sysORLastChange = null;

	private SNMPMonitor main;

	 public GroupMgmtGroupRH(SNMPInteractionToNexus nexus, SNMPMonitor monitor) {
		super(nexus);
		super.addOIDRange(baseOID+".1.1.*");
		super.addOIDRange(baseOID+".1.11.*");
		this.srte=nexus.getSrte();
		this.nexus=nexus;
		main = monitor;
		sysOREntries.add(new SysOREntry("1.3.6.1.2.1.1"," The System group: a collection of objects common to all managed systems",main.getSysUptime()));
		sysOREntries.add(new SysOREntry("1.3.6.1.2.1.11","The SNMP group: a collection of objects providing basic instrumentation and control of an SNMP entity",main.getSysUptime()));
		sysORLastChange = main.getSysUptime();
		
	}

	/**
     * @return The default implementation (MIB tree) will do fine, so just return null.
     * @see com.dvsoft.nexus.service.systemmanagement.snmp.SNMPTreeNexus
     */
	protected SNMPObjectIdentifier getNextOIDIfKnown(SNMPObjectIdentifier oid) {
		try {
			return calculateNextOIDForTable(oid, baseOID + ".1.1.9", 8, 4,sysOREntries.size());
		} catch (SNMPBadValueException e) {
			trace.severe("Error converting a String to a OID: ", e);
		}
		return null;
	}

	public SNMPObject getValueForOID(SNMPObjectIdentifier oid) throws SNMPGetException {
		//The system group
		if (oid.toString().startsWith(baseOID + ".1.1.1")) {//sysDesc
			return new SNMPOctetString(srte.getProductName() +
			        " version " + srte.getVersion() + ";" +
			        "Host name: " + srte.getHostName() + "; with SNMPv"+(main.getSnmpVersion()+1));
		}else if (oid.toString().startsWith(baseOID + ".1.1.2")) {//sysObjectID
			try {
				return new SNMPObjectIdentifier("1.3.6.1.4.1.1484");
			} catch (SNMPBadValueException e) {
				trace.log(Level.FINER,"Error getting sysObjectID",e);
			}
		}else if (oid.toString().startsWith(baseOID + ".1.1.3")) {//sysupTime
			return main.getSysUptime();
		}else if (oid.toString().startsWith(baseOID + ".1.1.4")) {//sysContact
			return new SNMPOctetString(sysContact);
		}else if (oid.toString().startsWith(baseOID + ".1.1.5")) {//sysName
			return new SNMPOctetString(sysName);
		}else if (oid.toString().startsWith(baseOID + ".1.1.6")) {//sysLocation
			return new SNMPOctetString(sysLocation);
		}else if (oid.toString().startsWith(baseOID + ".1.1.7")) {//sysServices
			return new SNMPInteger(64); //See SNMPv2 RFC
		}else if (oid.toString().startsWith(baseOID + ".1.1.8")) {
			return sysORLastChange;
		}if (oid.toString().startsWith(baseOID + ".1.1.9")) {  // Are we looking at the table?
			 int row = 0, column;
			 			 
			 if ((column = extractColumn(OID_COLUMN_INDEX_URI, oid)) > 0) {
				 row = extractRow(OID_ROW_INDEX_URI, oid) - 1;  // zero returned if not existant, 1 for first row (so have to -1 to zero-base it)
				 
				 if (row >= 0) { // row of -1 = reached the end...
					switch (column) {
					 	case 1: // sysORIndex
					 		return new SNMPInteger(row);
					 	case 2: // sysORID
					 		return ((SysOREntry)sysOREntries.elementAt(row)).getSysORID();
						case 3: // sysORDesc
					 		return ((SysOREntry)sysOREntries.elementAt(row)).getSysORDesc();
						case 4: // sysORDesc
					 		return ((SysOREntry)sysOREntries.elementAt(row)).getUptime();
	                    default:
	                    	return new SNMPOctetString("unknown column...");
	                    }
	                }
				 
			 }
		}
		
		//The snmp group
		if (oid.toString().startsWith(baseOID + ".1.11.30")) {//snmpEnableAuthenTraps This value indicates whether or not authenticationFailure traps are sent
			return new SNMPInteger(main.getSnmpEnableAuthenTrapsValue());
		}else if (oid.toString().startsWith(baseOID + ".1.11.31")) {//snmpSilentDrops not implemented
			return new SNMPCounter32();
		}else if (oid.toString().startsWith(baseOID + ".1.11.32")) {//snmpProxyDrops not implemented
			return new SNMPCounter32();
		}else if (oid.toString().startsWith(baseOID + ".1.11.1")) {//snmpInPkts
			return new SNMPCounter32(main.getSnmpInPktsCount());
		}else if (oid.toString().startsWith(baseOID + ".1.11.3")) {//snmpInBadVersions. The API does not include the needed methods to get the 
			return new SNMPCounter32(main.getSnmpInBadVersionsCount());
		}else if (oid.toString().startsWith(baseOID + ".1.11.4")) {//snmpInBadCommunityNames
			return new SNMPCounter32(main.getSnmpInBadCommunityNamesCount());
		}else if (oid.toString().startsWith(baseOID + ".1.11.5")) {//snmpInBadCommunityUses. Stong security not needed value is thus 0
			return new SNMPCounter32(0);
		}else if (oid.toString().startsWith(baseOID + ".1.11.6")) {//snmpInASNParseErrs
			return new SNMPCounter32(main.getSnmpInASNParseErrsCount());
		}
		return null;
	}

	public SNMPVarBindList setValueForOID(SNMPSequence pdu) throws SNMPSetException {
		SNMPSequence oidPair = (SNMPSequence) pdu.getSNMPObjectAt(0);
        SNMPObjectIdentifier oid = (SNMPObjectIdentifier) oidPair.getSNMPObjectAt(0);
        SNMPObject value = oidPair.getSNMPObjectAt(1);
        
        
        if (oid.toString().startsWith(baseOID + ".1.1.4")) {//sysContact
        	sysContact = new String((byte[])value.getValue());
        }else if (oid.toString().startsWith(baseOID + ".1.1.5")) {//sysName
        	sysName = new String((byte[])value.getValue());
        }else if (oid.toString().startsWith(baseOID + ".1.1.6")) {//sysLocation
        	sysLocation =new String((byte[])value.getValue());
        }else if (oid.toString().startsWith(baseOID + ".1.11.30")) {//snmpEnableAuthenTraps
        	main.setSnmpEnableAuthenTrapsValue(((BigInteger)value.getValue()).intValue());
        }else {
            throw new SNMPSetException("Setting is not supported for this OID", 0, SNMPRequestException.VALUE_READ_ONLY);
        }

        SNMPVarBindList ret = new SNMPVarBindList();
        try {
            ret.addSNMPObject(new SNMPVariablePair(oid, value));
        } catch (SNMPBadValueException e) {
            trace.severe("Unexpected value returned from seting value: [" + value + "]", e);
        }
        return ret;
		
	}

	protected void refreshSystemProperties() {
	}
	
	private class SysOREntry{
		
		private String sysORID;
		private String sysORDesc;
		private SNMPTimeTicks uptime;

		SysOREntry(String sysORID,String sysORDesc,SNMPTimeTicks uptime){
			this.sysORID =sysORID;
			this.sysORDesc = sysORDesc;
			this.uptime = uptime;
		}

		public SNMPOctetString getSysORDesc() {
			return new SNMPOctetString(sysORDesc);
		}

		public SNMPObjectIdentifier getSysORID() {
			try {
				return new SNMPObjectIdentifier (sysORID);
			} catch (SNMPBadValueException e) {
				trace.log(Level.FINER,"Error getting sysObjectID",e);
			}
			return null;
			
		}

		public SNMPTimeTicks getUptime() {
			return uptime;
		}
	}
	

}
